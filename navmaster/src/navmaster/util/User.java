package navmaster.util;

import java.util.Scanner;

public class User {

	Scanner s;
	
	public static final String YES = "yes";
	public static final String NO  = "no";
	
	public User() {
		s = new Scanner(System.in);
	}
	
	public String choose(String question, String... options) throws UserInteractionException {
		say(question, options);
		String scan;
		while((scan = s.nextLine()) != null) {
			for(String o : options) {
				if(scan.equals(o))
					return scan;
			}
			say("You need to provide one of the options", options);
		}
		throw new UserInteractionException("Expected a String but got null.");
	}
	
	public boolean ask(String question) throws UserInteractionException {
		String answer = choose(question, YES, NO);
		return YES.equals(answer);
	}
	
	public void confirm(String message) {
		say(message);
		s.nextLine();
	}
	
	public void say(String message) {
		System.out.println(message);
	}
	
	public void say(String message, String... options) {
		System.out.print(message + " [");
		for(int i = 0; i < options.length; i++) {
			System.out.print(options[i] + ", ");
		}
		System.out.println("]");
	}
	
}
