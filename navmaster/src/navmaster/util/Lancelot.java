package navmaster.util;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import jssc.SerialPortException;
import navmaster.algorithm.MoveBackwardAlgorithm;
import navmaster.algorithm.MoveForwardAlgorithm;
import navmaster.tank.SerialControlledTank;
import navmaster.tank.TankConfiguration;
import navmaster.tank.TankConfiguration.InvalidConfigurationException;

public class Lancelot extends JFrame {

	private static final long serialVersionUID = -6262977375945040702L;

	public static final void main(String[] args) {
		Lancelot lancelot = new Lancelot();
		lancelot.setVisible(true);
	}
	
	private JSpinner motorSpeedSpinner;
	private JSpinner leftMotorFactorSpinner;
	private JSpinner rightMotorFactorSpinner;
	private JSpinner rtlFactorSpinner;
	private JSpinner rtrFactorSpinner;
	private JSpinner ltlFactorSpinner;
	private JSpinner ltrFactorSpinner;
	private JSpinner distanceSpinner;
	
	private JTextField serialDeviceTextField;
	private JLabel serialConnectionLabel;
	private JButton serialCloseButton;
	private JButton serialOpenButton;
	private JButton serialUploadButton;
	
	private SerialControlledTank tank;
	
	public Lancelot() {
		super("Lancelot :: Satank Engineer");
		
		setLayout(new BorderLayout());
		
		JPanel contentsPanel = new JPanel();
		contentsPanel.setLayout(new GridLayout(2, 1));
		
		JPanel inputsPanel = new JPanel();
		inputsPanel.setLayout(new GridLayout(8, 2));
		JLabel motorSpeedLabel = new JLabel("Motor Speed");
		motorSpeedSpinner = new JSpinner();
		motorSpeedSpinner.setValue(75);
		
		JLabel leftMotorFactorLabel = new JLabel("Left-hand motor factor");
		leftMotorFactorSpinner = newFactorSpinner();
		
		JLabel rightMotorFactorLabel = new JLabel("Right-hand motor factor");
		rightMotorFactorSpinner = newFactorSpinner();

		JLabel rtlFactorLabel = new JLabel("Right motor turning left factor");
		rtlFactorSpinner = newFactorSpinner();
		
		JLabel rtrFactorLabel = new JLabel("Right motor turning right factor");
		rtrFactorSpinner = newFactorSpinner();
		
		JLabel ltlFactorLabel = new JLabel("Left motor turning left factor");
		ltlFactorSpinner = newFactorSpinner();
		
		JLabel ltrFactorLabel = new JLabel("Left motor turning right factor");
		ltrFactorSpinner = newFactorSpinner();
		
		JLabel moveDistLabel = new JLabel("GO! distance");
		distanceSpinner = newFactorSpinner();
		
		inputsPanel.add(motorSpeedLabel);
		inputsPanel.add(motorSpeedSpinner);
		inputsPanel.add(leftMotorFactorLabel);
		inputsPanel.add(leftMotorFactorSpinner);
		inputsPanel.add(rightMotorFactorLabel);
		inputsPanel.add(rightMotorFactorSpinner);
		inputsPanel.add(rtlFactorLabel);
		inputsPanel.add(rtlFactorSpinner);
		inputsPanel.add(rtrFactorLabel);
		inputsPanel.add(rtrFactorSpinner);
		inputsPanel.add(ltlFactorLabel);
		inputsPanel.add(ltlFactorSpinner);
		inputsPanel.add(ltrFactorLabel);
		inputsPanel.add(ltrFactorSpinner);
		inputsPanel.add(moveDistLabel);
		inputsPanel.add(distanceSpinner);
		contentsPanel.add(inputsPanel);
		
		JPanel connectionPanel = new JPanel();
		connectionPanel.setLayout(new GridLayout(3, 1));
		
		serialDeviceTextField = new JTextField("Serial Device Name (/dev/ttyACM0 or COM-1 etc.)");
		serialConnectionLabel = new JLabel("Not connected to serial port");
		
		JPanel serialButtonsPanel = new JPanel();
		serialButtonsPanel.setLayout(new GridLayout(1, 3));
		
		serialOpenButton = new JButton("Connect");
		serialOpenButton.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent arg0) {
				openSerialConnection();
			}
		});
		serialCloseButton = new JButton("Close Connection");
		serialCloseButton.setEnabled(false);
		serialCloseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closeSerialConnection();
			}
		});
		serialUploadButton = new JButton("Upload");
		serialUploadButton.setEnabled(false);
		serialUploadButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				uploadSettings();
			}
		});
		
		serialButtonsPanel.add(serialOpenButton);
		serialButtonsPanel.add(serialCloseButton);
		serialButtonsPanel.add(serialUploadButton);
		
		connectionPanel.add(serialDeviceTextField);
		connectionPanel.add(serialButtonsPanel);
		connectionPanel.add(serialConnectionLabel);
		contentsPanel.add(connectionPanel);
		
		tank = null;
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (tank != null) {
					closeSerialConnection();
					System.exit(0);
				}
			}
		});
		
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		
		JMenuItem openItem = new JMenuItem("Open");
		openItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				openFile();
			}
		});
		JMenuItem saveItem = new JMenuItem("Save");
		saveItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveFile();
			}
		});
		
		fileMenu.add(openItem);
		fileMenu.add(saveItem);

		menuBar.add(fileMenu);

		JMenu goMenu = new JMenu("GO!");
		JMenuItem fwdI = new JMenuItem("FORWARD");
		fwdI.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new MoveForwardAlgorithm(tank, ((Double) distanceSpinner.getValue()).intValue()).runAlgorithm();
			}
		});
		
		JMenuItem rvrs = new JMenuItem("REVERSE");
		rvrs.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new MoveBackwardAlgorithm(tank, ((Double) distanceSpinner.getValue()).intValue()).runAlgorithm();
			}
		});
		
		JMenuItem leftTurn = new JMenuItem("TURN LEFT");
		leftTurn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				tank.turnLeft(((Double) distanceSpinner.getValue()).intValue());
			}
		});
		
		
		JMenuItem rightTurn = new JMenuItem("TURN RIGHT");
		rightTurn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				tank.turnRight(((Double) distanceSpinner.getValue()).intValue());
			}
		});
		
		
		goMenu.add(fwdI);
		goMenu.add(rvrs);
		goMenu.add(leftTurn);
		goMenu.add(rightTurn);
		menuBar.add(goMenu);
		
		add(menuBar, BorderLayout.NORTH);
		add(contentsPanel, BorderLayout.CENTER);
		pack();
	}
	
	private JSpinner newFactorSpinner() {
		return new JSpinner(new SpinnerNumberModel(1.0, 0.0, 100000.0, 0.01));
	}
	
	private TankConfiguration generateConfiguration() throws InvalidConfigurationException {
		TankConfiguration config = new TankConfiguration(
				(int) motorSpeedSpinner.getValue(), 
				Float.parseFloat(rightMotorFactorSpinner.getValue().toString()), 
				Float.parseFloat(leftMotorFactorSpinner.getValue().toString()), 
				Float.parseFloat(rtlFactorSpinner.getValue().toString()), 
				Float.parseFloat(rtrFactorSpinner.getValue().toString()), 
				Float.parseFloat(ltlFactorSpinner.getValue().toString()), 
				Float.parseFloat(ltrFactorSpinner.getValue().toString()));
				// I'm convinced there's a nicer way to do that conversion ...
		return config;
	}
	
	private void openSerialConnection() {		
		try {
			tank = new SerialControlledTank(serialDeviceTextField.getText());
			serialConnectionLabel.setText("Connected to tank");
			
			serialUploadButton.setEnabled(true);
			serialCloseButton.setEnabled(true);
			serialOpenButton.setEnabled(false);
			
		} catch (SerialPortException e) {
			serialConnectionLabel.setText("Connection failed: " + e.getMessage());
		}
	}
	
	private void closeSerialConnection() {
		try {
			tank.closeConnection();
			tank = null;
			serialConnectionLabel.setText("Connection closed");
			
			serialUploadButton.setEnabled(false);
			serialCloseButton.setEnabled(false);
			serialOpenButton.setEnabled(true);
		} catch (SerialPortException e) {
			serialConnectionLabel.setText("Connection failed to close");
		}
	}
	
	private void uploadSettings() {
		try {
			serialConnectionLabel.setText("Uploading configuration");
			update(getGraphics());
			TankConfiguration config = generateConfiguration();
			tank.uploadConfiguration(config);
			serialConnectionLabel.setText("Configuration uploaded!");
		} catch (InvalidConfigurationException e) {
			JOptionPane.showMessageDialog(this,
				    e.getMessage(),
				    "Invalid configuration",
				    JOptionPane.ERROR_MESSAGE);
		}
	}

	private void openFile() {
		JFileChooser chooser = new JFileChooser();
		int result = chooser.showOpenDialog(this);
		
		if (result == JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			
			try ( InputStream fileStream = new FileInputStream(file);
			      InputStream buffer = new BufferedInputStream(fileStream);
			      ObjectInput input = new ObjectInputStream (buffer); ) {
				TankConfiguration config = (TankConfiguration) input.readObject();
				displayConfig(config);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this,
					    e.getMessage(),
					    "Open configuration failed",
					    JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	private void displayConfig(TankConfiguration config) {
		motorSpeedSpinner.setValue(config.motorSpeed);
		rightMotorFactorSpinner.setValue(config.rightMotorFactor);
		leftMotorFactorSpinner.setValue(config.leftMotorFactor);
		rtlFactorSpinner.setValue(config.rtlMotorFactor);
		rtrFactorSpinner.setValue(config.rtrMotorFactor);
		ltlFactorSpinner.setValue(config.ltlMotorFactor);
		ltrFactorSpinner.setValue(config.ltrMotorFactor);
	}
	
	private void saveFile() {
		JFileChooser chooser = new JFileChooser();
		int result = chooser.showOpenDialog(this);
		
		if (result == JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			
			try (
				OutputStream fileStream = new FileOutputStream(file);
				OutputStream buffer = new BufferedOutputStream(fileStream);
				ObjectOutput out = new ObjectOutputStream(buffer); ) {
				out.writeObject(generateConfiguration());
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this,
					    e.getMessage(),
					    "Save configuration failed",
					    JOptionPane.ERROR_MESSAGE);
			}
		} 
	}
	
}
