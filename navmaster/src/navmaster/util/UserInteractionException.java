package navmaster.util;

public class UserInteractionException extends Exception {

	private static final long serialVersionUID = 824726541699380892L;

	public UserInteractionException(String string) {
		super(string);
	}
}
