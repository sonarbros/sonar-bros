package navmaster.util;

import jssc.SerialPortException;
import navmaster.tank.SerialControlledTank;

public class LancelotCli {

	private static final String PLACE_TANK_BACKWARDS = "Please place the tank on the *backwards* starting position and hit enter to start.";
	public static final String PLACE_TANK_ON_START = "Please place the tank on the starting position and hit enter to confirm.";
	private static final String LEFT = "left";
	private static final String RIGHT = "right";
	private static final String STRAIGHT = "straight";
	private static final String TOOFAR = "toofar";
	private static final String NOTENOUGH = "notenough";
	private static final String JUSTRIGHT = "justright";
	private static final String NOTSURE = "notsure";
	private static final String TINYTOOMUCH = "abittoomuch";
	private static final String TINYNOTENOUGH = "notquiteneough";
	
	private User u;
	private SerialControlledTank t;
	private Mode m;
	
	private static final int FWD_BK_MV = 120;
	private static final int TRN_MV = 200;
	
	private int moveSpeed;
	private float tLM;
	private float tRM;
	
	private int unitsForward;
	private int unitsBackward;
	private int unitsLeft;
	private int unitsRight;
	
	private enum Mode {
		WELCOME("welcome"),
		LEFTRIGHT("leftright"),
		FORWARD("forward"),
		BACKWARD("backward"),
		LEFT("left"),
		RIGHT("right"),
		LEFT_WITH_SERVO("leftwithservo"),
		DONE("done");
		
		private final String s;
		
		private Mode(String s) {
			this.s = s;
		}
		
		public String getString() {
			return s;
		}
		
		public static Mode getModeForString(String s) {
			for(Mode m : Mode.values()) {
				if(m.getString().equals(s)){
					return m;
				}
			}
			return WELCOME;
		}
	}

	public static void main(String... args) {
		if(args.length == 0)
			return;
		try {
			LancelotCli lc;
			if(args.length == 1) {
				lc = new LancelotCli(args[0]);
			} else {
				lc = new LancelotCli(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]), Float.parseFloat(args[5]), Float.parseFloat(args[6]));
			}
			
			lc.setFactors();
			lc.calibrate();
		} catch (Throwable t) {
			t.printStackTrace();
			System.out.println(t.getMessage());
		}
	}
	
	public LancelotCli(String serialAddress) throws SerialPortException, UserInteractionException {
		this(serialAddress, 1000, 1000, 1000, 1000, 1, 1);
	}
	
	public LancelotCli(String serialAddress, int unitsForward, int unitsBackward,
			int unitsLeft, int unitsRight, float tLM, float tRM) throws SerialPortException {
		u = new User();
		t = new SerialControlledTank(serialAddress);
		m = Mode.WELCOME;
		moveSpeed = FWD_BK_MV;
		this.tLM = tLM;
		this.tRM = tRM;
		this.unitsForward = unitsForward;
		this.unitsBackward = unitsBackward;
		this.unitsLeft = unitsLeft;
		this.unitsRight = unitsRight;
	}

	private void setFactors() {
		if(t != null) {
			t.setSpeed(moveSpeed);
			t.changeFactor(SerialControlledTank.LEFT_SPEED_FACTOR, tLM);
			t.changeFactor(SerialControlledTank.RIGHT_SPEED_FACTOR, tRM);
		}
	}
	
	private String[] modes() {
		Mode[] modes = Mode.values();
		String[] names = new String[modes.length];
		for(int i = 0; i < modes.length; i++) {
			names[i] = modes[i].getString();
		}
		return names;
	}
	
	public void calibrate() throws UserInteractionException {
		while(true) {
			switch (m) {
			case WELCOME:
				u.say("Welcome to the Lancelot Calibration Wizard");
				m = Mode.getModeForString(u.choose("Select a test", modes()));
				switch(m) {
				case FORWARD:
				case LEFTRIGHT:
				case BACKWARD:
					moveSpeed = FWD_BK_MV;
					break;
				case LEFT:
				case RIGHT:
				case LEFT_WITH_SERVO:
					moveSpeed = TRN_MV;
					break;
				case WELCOME:
				case DONE:
					break;
				}
				continue;
			case LEFTRIGHT:
				leftright();
				continue;
			case FORWARD:
				forward();
				continue;
			case BACKWARD:
				backward();
				continue;
			case LEFT:
				left();
				continue;
			case RIGHT:
				right();
				continue;
			case LEFT_WITH_SERVO:
				left_with_servo();
				continue;
			case DONE:
				System.out.println("Units Forward " + unitsForward);
				System.out.println("Units Backward " + unitsBackward);
				System.out.println("Units Left " + unitsLeft);
				System.out.println("Units Right " + unitsRight);
				System.out.println("Left Motor Factor " + tLM);
				System.out.println("Right Motor Factor " + tRM);
				System.out.println(unitsForward + " " + unitsBackward + " " + unitsLeft + " " + unitsRight + " " + tLM + " " + tRM);
				return;
			}
		}
	}
	
	private void left_with_servo() throws UserInteractionException {
		u.confirm(PLACE_TANK_ON_START);
		t.setCaster(true);
		t.turnLeft(unitsLeft);
		t.setCaster(true);
		String answer = u.choose("Did the tank move:", JUSTRIGHT, TOOFAR, NOTENOUGH, TINYNOTENOUGH, TINYTOOMUCH, NOTSURE);
		switch(answer) {
		case JUSTRIGHT:
			m = Mode.WELCOME;
			break;
		case NOTENOUGH:
			unitsLeft *= 1.1;
			break;
		case TOOFAR:
			unitsLeft *= 0.9;
			break;
		case TINYTOOMUCH:
			unitsLeft *= 0.95;
		case TINYNOTENOUGH:
			unitsLeft *= 1.05;
		case NOTSURE:
			break;
		}
	}

	private void left() throws UserInteractionException {
		u.confirm(PLACE_TANK_ON_START);
		t.turnLeft(unitsLeft);
		String answer = u.choose("Did the tank move:", JUSTRIGHT, TOOFAR, NOTENOUGH, NOTSURE);
		switch(answer) {
		case JUSTRIGHT:
			m = Mode.WELCOME;
			break;
		case NOTENOUGH:
			unitsLeft *= 1.1;
			break;
		case TOOFAR:
			unitsLeft *= 0.9;
			break;
		case NOTSURE:
			break;
		}
	}
	
	private void right() throws UserInteractionException {
		u.confirm(PLACE_TANK_ON_START);
		t.turnRight(unitsRight);
		String answer = u.choose("Did the tank move:", JUSTRIGHT, TOOFAR, NOTENOUGH, NOTSURE);
		switch(answer) {
		case JUSTRIGHT:
			m = Mode.WELCOME;
			break;
		case NOTENOUGH:
			unitsRight *= 1.1;
			break;
		case TOOFAR:
			unitsRight *= 0.9;
			break;
		case NOTSURE:
			break;
		}
	}
	
	private void leftright() throws UserInteractionException {
		u.confirm(PLACE_TANK_ON_START);
		t.moveForward(unitsForward);
		String answer = u.choose("Did the tank move:", STRAIGHT, LEFT, RIGHT, NOTSURE);
		switch(answer) {
		case LEFT:
			tLM *= 1.1;
			break;
		case RIGHT:
			tLM *= 0.9;
			break;
		case STRAIGHT:
			m = Mode.WELCOME;
			break;
		case NOTSURE:
			break;
		}
		setFactors();
	}
	
	private void forward() throws UserInteractionException {
		u.confirm(PLACE_TANK_ON_START);
		t.moveForward(unitsForward);
		String answer = u.choose("Did the tank move:", JUSTRIGHT, TOOFAR, NOTENOUGH, NOTSURE);
		switch(answer) {
		case JUSTRIGHT:
			m = Mode.WELCOME;
			break;
		case NOTENOUGH:
			unitsForward *= 1.1;
			break;
		case TOOFAR:
			unitsForward *= 0.9;
			break;
		case NOTSURE:
			break;
		}
	}
	
	private void backward() throws UserInteractionException {
		u.confirm(PLACE_TANK_BACKWARDS);
		t.moveBackward(unitsBackward);
		String answer = u.choose("Did the tank move:", JUSTRIGHT, TOOFAR, NOTENOUGH, NOTSURE);
		switch(answer) {
		case JUSTRIGHT:
			m = Mode.WELCOME;
			break;
		case NOTENOUGH:
			unitsBackward *= 1.1;
			break;
		case TOOFAR:
			unitsBackward *= 0.9;
			break;
		case NOTSURE:
			break;
		}
	}
	
}
