package navmaster;

import navmaster.algorithm.Algorithm;
import navmaster.algorithm.AutonomousMappingAlgorithm;
import navmaster.algorithm.ContinuousSensorReadAlgorithm;
import navmaster.algorithm.RunOnTheSpotAlgorithm;
import navmaster.algorithm.SensorTestAlgorithm;
import navmaster.algorithm.TransmitTestAlgorithm;
import navmaster.tank.SerialControlledTank;
import navmaster.tank.Tank;

public class Main {

	public static void main(String[] args) {
		Algorithm algo = null;
	
		if (args.length != 2 && args.length != 8) {
			System.out.println("Usage navmaster <serial device> <renderer host>");
			System.exit(1);
		}
		
		String serialDevice = args[0];
		String renderHost = args[1];
		
		System.out.println("Using device " + serialDevice + ", rendering to " + renderHost);
		
		try {
			SerialControlledTank tank = new SerialControlledTank(serialDevice);

			//algo = new RunOnTheSpotAlgorithm(tank);
			//algo = new ContinuousSensorReadAlgorithm(tank);
			if(args.length == 2) {
				algo = new AutonomousMappingAlgorithm(tank, renderHost, 60833);
			} else {
				algo = new AutonomousMappingAlgorithm(tank, renderHost, 60833, Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]), Integer.parseInt(args[5]), Float.parseFloat(args[6]), Float.parseFloat(args[7]));
			}
		} catch (Exception e) {
			System.out.println("Setup failed: " + e);
			System.exit(1);
		}
		
		algo.runAlgorithm();
	}

}
