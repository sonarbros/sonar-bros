package navmaster.algorithm;

import java.io.IOException;

import navmaster.tank.Tank;
import navmaster.tx.SocketTransmit;

public abstract class TransmittingAlgorithm extends Algorithm {

	private SocketTransmit tx;
	
	public TransmittingAlgorithm(Tank t, String addr, int port) throws IOException {
		super(t);
		tx = new SocketTransmit(addr, port);
	}
	
	protected void tx(int x, int y) {
		tx.transmitPoint(x, y);
	}
	
	protected void tx(double x, double y) {
		tx.transmitPoint(x, y);
	}
	
}
