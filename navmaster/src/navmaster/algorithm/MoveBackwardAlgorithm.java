package navmaster.algorithm;

import navmaster.tank.Tank;

public class MoveBackwardAlgorithm extends Algorithm {
	
	int n;
	boolean infinite;
	
	public MoveBackwardAlgorithm(Tank tank) {
		super(tank);
		infinite = true;
		n = 1;
	}
	
	public MoveBackwardAlgorithm(Tank tank, int n) {
		super(tank);
		this.n = n;
		infinite = false;
	}

	@Override
	public void runAlgorithm() {
		do {
			tank.moveBackward(n);
		} while (infinite);
	}

}
