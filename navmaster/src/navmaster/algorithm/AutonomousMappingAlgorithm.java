package navmaster.algorithm;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import navmaster.tank.QuadSensorDirections;
import navmaster.tank.SerialControlledTank;
import navmaster.tank.Tank;
import navmaster.tank.TankConfiguration;

public class AutonomousMappingAlgorithm extends TransmittingAlgorithm {
	
	private final static int READS_PER_SPOT = 1;
	private final static double CM_PER_UNIT = 7;
	private final static double ANGLE_PER_TURN = 90;

	private final static int TANK_FWD_SPEED = 120;
	private final static int TANK_TURN_SPEED = 200;
	private static final double SPEED_OF_SOUND = 0.034; // cm per microsecond
	
	private double currentX;
	private double currentY;
	private double currentAngle;
	
	private float tLM;
	private float tRM;
	
	private int unitsForward;
	private int unitsBackward;
	private int unitsLeft;
	private int unitsRight;
	
	public AutonomousMappingAlgorithm(SerialControlledTank t, String addr, int port)
			throws IOException {
		this(t, addr, port, 1000, 1000, 1000, 1000, 1, 1);
	}

	public AutonomousMappingAlgorithm(SerialControlledTank t, String addr, int port,
			int unitsForward, int unitsBackward,
			int unitsLeft, int unitsRight, float tLM, float tRM) throws IOException {
		super(t, addr, port);
		currentX = 0;
		currentY = 0;
		currentAngle = 0;
		this.unitsForward = unitsForward;
		this.unitsBackward = unitsBackward;
		this.unitsLeft = unitsLeft;
		this.unitsRight = unitsRight;
		t.changeFactor(SerialControlledTank.LEFT_SPEED_FACTOR, tLM);
		t.changeFactor(SerialControlledTank.RIGHT_SPEED_FACTOR, tRM);
	}

	@Override
	public void runAlgorithm() {	
		//uploadLancelotConfig("/tmp/tank-config");//TODO:Remove all lancelot 1 calls, should all be lancelot 2 now which isn't file based.
		
		/*
		while (true) {
			lookAround();
			
			if (!safeMoveForwards()) {
				turnLeft(TANK_TURN_45DEG);
			}
			
			while (safeMoveForwards()) {
				lookLeftRight();
			}
		}*/
		
		txPoints(readAllSensors());
		int turnCount = 0;
		
		while (turnCount < 4) {
			currentAngle %= 360;
			while (safeMoveForwards()) {
				txPoints(readLeftRightSensors());
			}
			
			txPoints(readAllSensors());
			List<Point> lr = readLeftRightSensors();
			Point current = new Point(currentX, currentY);
			if(current.distanceTo(lr.get(0)) < current.distanceTo(lr.get(1))){
				turnRight(unitsRight);
				currentAngle += ANGLE_PER_TURN;
			} else {
				turnLeft(unitsLeft);
				currentAngle -= ANGLE_PER_TURN;
			}
			turnCount++;
		}
	}
	
	private void turnLeft(int units) {
		tank.setSpeed(TANK_TURN_SPEED);
		tank.turnLeft(units);
	}
	
	private void turnRight(int units) {
		tank.setSpeed(TANK_TURN_SPEED);
		tank.turnRight(units);
	}
	
	private void uploadLancelotConfig(String path) {
		File file = new File(path);
		try ( InputStream fileStream = new FileInputStream(file);
			      InputStream buffer = new BufferedInputStream(fileStream);
			      ObjectInput input = new ObjectInputStream (buffer); ) {
				TankConfiguration config = (TankConfiguration) input.readObject();
				((SerialControlledTank) tank).uploadConfiguration(config);
			} catch (Exception e) {
				System.out.println("Failed to upload lancelot configuration");
			}
	}
	
	private boolean safeMoveForwards() {
		double unitsInFront = getAveragedReading(QuadSensorDirections.FRONT);

		System.out.println(String.format("Units in front: %s", unitsInFront));
		if (unitsInFront >= 1) {
			moveForward(unitsForward);
			return true;
		} else {
			return false;
		}
	}
	
	private boolean safeMoveBackwards() {
		double unitsBehind = getAveragedReading(QuadSensorDirections.BACK);
		
		if (unitsBehind >= 1) {
			moveBackward(unitsBackward);
			return true;
		} else {
			return false;
		}
	}
	
	private void moveForward(int units) {
		tank.setSpeed(TANK_FWD_SPEED);
		tank.moveForward(units);
		
		currentX += ((Math.cos(Math.toRadians(currentAngle)) * units) / unitsForward);
		currentY += ((Math.sin(Math.toRadians(currentAngle)) * units) / unitsForward);
	}
	
	private void moveBackward(int units) {
		tank.setSpeed(TANK_FWD_SPEED);
		tank.moveBackward(units);
		
		currentX += ((Math.cos(Math.toRadians(currentAngle + 180)) * units) / unitsBackward);
		currentY += ((Math.sin(Math.toRadians(currentAngle + 180)) * units) / unitsBackward);
	}
	
	private void lookAround() {
		for (int i = 0; i < (360 / ANGLE_PER_TURN); i++) {
			turnLeft(unitsLeft);
			currentAngle = (currentAngle - ANGLE_PER_TURN) % 360;
			txPoints(readAllSensors());
		}
	}
	
	private void lookLeftRight() {
		txPoints(readLeftRightSensors());
	}
	
	protected List<Point> readAllSensors() {
		List<Point> points = new ArrayList<>();
		
		for (int i = 0; i < READS_PER_SPOT; i++) {
			for (QuadSensorDirections d : QuadSensorDirections.values()) {
				points.add(calculatePoint(getAveragedReading(d), d));
			}
		}

		return points;
	}
	
	protected List<Point> readLeftRightSensors() {
		List<Point> points = new ArrayList<>();
		
		for (int i = 0; i < READS_PER_SPOT; i++) {
			points.add(calculatePoint(getAveragedReading(QuadSensorDirections.LEFT), QuadSensorDirections.LEFT));
			points.add(calculatePoint(getAveragedReading(QuadSensorDirections.RIGHT), QuadSensorDirections.RIGHT));
		}
		
		return points;
	}
	
	private Point calculatePoint(double units, QuadSensorDirections d) {
		double x = 0;
		double y = 0;
		System.out.println(d.getUsefulName() + " " + units);
		// FIXME: Not quite right because the center is in the wrong place
		switch (d) {
		case FRONT:
			x = currentX + units * Math.cos(Math.toRadians(currentAngle));
			y = currentY + units * Math.sin(Math.toRadians(currentAngle));
			break;
		case BACK:
			x = currentX + units * Math.cos(Math.toRadians(currentAngle + 180));
			y = currentY + units * Math.sin(Math.toRadians(currentAngle + 180));
			break;
		case LEFT:
			x = currentX + units * Math.cos(Math.toRadians(currentAngle - 90));
			y = currentY + units * Math.sin(Math.toRadians(currentAngle - 90));
			break;
		case RIGHT:
			x = currentX + units * Math.cos(Math.toRadians(currentAngle + 90));
			y = currentY + units * Math.sin(Math.toRadians(currentAngle + 90));
			break;
		}
		
		return new Point(x, y);
	}
	
	/**
	 * Left/right well 14cm maybe ish
	 * Back / front 16cm
	 */
	private static int getSensorOffset(QuadSensorDirections d) {
		if(true)
			return 0;
			
		switch (d) {
		case FRONT:
			return -466;
		case BACK:
			return 466;
		case LEFT:
			return -407;
		case RIGHT: 
			return 407;
		}
		
		return 0;
	}
	
	private double getAveragedReading(QuadSensorDirections dir) {
		double reading = tank.getSensorReading(dir);
		return (SPEED_OF_SOUND * (reading - getSensorOffset(dir))) / CM_PER_UNIT;
	}
	
	private void txPoints(List<Point>  points) {
		for (Point p : points) {
			tx(p.x, p.y);
		}
	}
	
 	class Point {
		public final double x;
		public final double y;
		
		public Point(double x, double y) {
			this.x = x;
			this.y = y;
		}
		
		public double distanceTo(Point p) {
			return Math.sqrt(Math.pow(x-p.x, 2) + Math.pow(y-p.y, 2));
		}
		
	}
	
}