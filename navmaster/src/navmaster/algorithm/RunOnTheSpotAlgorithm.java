package navmaster.algorithm;

import navmaster.tank.QuadSensorDirections;
import navmaster.tank.SensorDirection;
import navmaster.tank.Tank;

public class RunOnTheSpotAlgorithm extends Algorithm {

	public RunOnTheSpotAlgorithm(Tank tank) {
		super(tank);

		tank.setSpeed(124);
	}
	

	@Override
	public void runAlgorithm() {
		while (true) {
			tank.setSensorAngles(45);
			tank.moveForward(1);
			tank.moveBackward(1);
			tank.turnLeft(1);
			tank.turnRight(1);
			tank.setSensorAngles(90);
			
			//tank.getSensorReading(QuadSensorDirections.FRONT);
			for (SensorDirection dir : QuadSensorDirections.values()) {
				double reading = tank.getSensorReading(dir);	
				System.out.println(dir.getUsefulName() + ": " + reading);
			}
		}
	}
	
}
