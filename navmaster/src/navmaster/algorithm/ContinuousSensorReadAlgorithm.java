package navmaster.algorithm;

import navmaster.tank.QuadSensorDirections;
import navmaster.tank.Tank;

public class ContinuousSensorReadAlgorithm extends Algorithm {

	public ContinuousSensorReadAlgorithm(Tank tank) {
		super(tank);
	}

	@Override
	public void runAlgorithm() {
		while (true) {
			System.out.println(tank.getSensorReading(QuadSensorDirections.LEFT));
		}
	}

}
