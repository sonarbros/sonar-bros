package navmaster.algorithm;

import java.io.IOException;

import navmaster.tank.Tank;

public class TransmitTestAlgorithm extends TransmittingAlgorithm {

	public TransmitTestAlgorithm(Tank t) throws IOException {
		super(t, "localhost", 60833);
	}

	@Override
	public void runAlgorithm() {
		tx(10,10);
		tx(10,15);
		tx(10,20);
		tx(20,10);
	}

}
