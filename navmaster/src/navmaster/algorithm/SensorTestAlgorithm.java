package navmaster.algorithm;

import java.io.IOException;

import navmaster.tank.SerialControlledTank;
import navmaster.tank.Tank;

public class SensorTestAlgorithm extends AutonomousMappingAlgorithm {

	public SensorTestAlgorithm(SerialControlledTank t, String addr, int port)
			throws IOException {
		super(t, addr, port);

	}

	@Override
	public void runAlgorithm() {
		while (true) {
			for (Point p : readLeftRightSensors()) {
				System.out.print("(" + p.x + ", " + p.y + ") ");
				tx(p.x, p.y);
			}
			
			System.out.println();
		}
	}
	
}
