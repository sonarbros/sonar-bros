package navmaster.algorithm;

import navmaster.tank.Tank;

public abstract class Algorithm {

	protected Tank tank;
	
	public Algorithm(Tank tank) {
		this.tank = tank;
	}
	
	public abstract void runAlgorithm();
}
