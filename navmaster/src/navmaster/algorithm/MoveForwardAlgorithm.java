package navmaster.algorithm;

import navmaster.tank.Tank;

public class MoveForwardAlgorithm extends Algorithm {
	
	int n;
	boolean infinite;
	
	public MoveForwardAlgorithm(Tank tank) {
		super(tank);
		infinite = true;
		n = 1;
	}
	
	public MoveForwardAlgorithm(Tank tank, int n) {
		super(tank);
		this.n = n;
		infinite = false;
	}

	@Override
	public void runAlgorithm() {
		do {
			tank.moveForward(n);
		} while (infinite);
	}

}
