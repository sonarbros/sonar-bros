package navmaster.tx;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketTransmit {

	private Socket sock;
	private PrintWriter out;
	
	public SocketTransmit(String addr, int port) throws IOException {
		sock = new Socket(addr, port);
		out = new PrintWriter(sock.getOutputStream(), true);
	}
	
	public void transmitPoint(int x, int y) {
		out.write(x + "," + y + "\n");
		out.flush();
	}
	
	public void transmitPoint(double x, double y) {
		out.write(x + "," + y + "\n");
		out.flush();
	}
	
	
}
