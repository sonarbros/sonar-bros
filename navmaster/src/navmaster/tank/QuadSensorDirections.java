package navmaster.tank;

public enum QuadSensorDirections implements SensorDirection {
	FRONT("front"),
	LEFT("left"),
	RIGHT("right"),
	BACK("back");
	
	private String usefulName;
	
	private QuadSensorDirections(String usefulName) {
		this.usefulName = usefulName;
	}
	
	public String getUsefulName() {
		return usefulName;
	}

}
