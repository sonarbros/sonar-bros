package navmaster.tank;

public interface Tank {
	public void moveForward(int units);
	public void moveBackward(int units);
	public void turnRight(int units);
	public void turnLeft(int units);
	public double getSensorReading(SensorDirection sensor);
	public void setSensorAngles(int angle);
	public void setSpeed(int speed);
}
