package navmaster.tank;

import java.io.Serializable;

public class TankConfiguration implements Serializable {

	private static final long serialVersionUID = -4194859522193542559L;
	public int motorSpeed;
	public float rightMotorFactor;
	public float leftMotorFactor;
	public float rtlMotorFactor;
	public float rtrMotorFactor;
	public float ltlMotorFactor;
	public float ltrMotorFactor;
	
	public TankConfiguration(int motorSpeed, float rightMotorFactor,
			float leftMotorFactor, float rtlMotorFactor, float rtrMotorFactor,
			float ltlMotorFactor, float ltrMotorFactor)
				throws InvalidConfigurationException {
		super();
		this.motorSpeed = motorSpeed;
		this.rightMotorFactor = rightMotorFactor;
		this.leftMotorFactor = leftMotorFactor;
		this.rtlMotorFactor = rtlMotorFactor;
		this.rtrMotorFactor = rtrMotorFactor;
		this.ltlMotorFactor = ltlMotorFactor;
		this.ltrMotorFactor = ltrMotorFactor;
		
		verify();
	}
	
	private void verify() throws InvalidConfigurationException {
		if (motorSpeed <= 0 || motorSpeed > 255) {
			throw new InvalidConfigurationException("Invalid motor speed (0 <= n < 255)");
		}
		
		verifyFactor(rightMotorFactor, "right motor factor");
		verifyFactor(leftMotorFactor, "left motor factor");
		verifyFactor(rtlMotorFactor, "right turning left motor factor");
		verifyFactor(rtrMotorFactor, "right turning right motor factor");
		verifyFactor(ltlMotorFactor, "left turning left motor factor");
		verifyFactor(ltrMotorFactor, "left turning right motor factor");
	}
	
	private void verifyFactor(float factor, String desc) 
			throws InvalidConfigurationException {
		if (!isValidFactor(factor)) {
			throw new InvalidConfigurationException("Invalid " + desc);
		}
	}
	
	private boolean isValidFactor(float factor) {
		float result = factor * motorSpeed;
		return (result > 0 && result <= 255);
	}
	
	public class InvalidConfigurationException extends Exception {
		
		private static final long serialVersionUID = 9057035907572788048L;

		public InvalidConfigurationException(String msg) {
			super(msg);
		}
		
	}
	
}
