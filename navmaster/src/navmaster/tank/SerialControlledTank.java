package navmaster.tank;

import jssc.SerialPort;
import jssc.SerialPortException;


public class SerialControlledTank implements Tank {

	public static final String LEFT_MOTOR_TURNING_RIGHT = "leftMotorTurningRight";
	public static final String LEFT_MOTOR_TURNING_LEFT = "leftMotorTurningLeft";
	public static final String RIGHT_MOTOR_TURNING_RIGHT = "rightMotorTurningRight";
	public static final String RIGHT_MOTOR_TURNING_LEFT = "rightMotorTurningLeft";
	public static final String LEFT_SPEED_FACTOR = "leftSpeedFactor";
	public static final String RIGHT_SPEED_FACTOR = "rightSpeedFactor";
	private final static int SAMPLE_SIZE = 50;
	private String serialAddress;
	private SerialPort serialPort;
	private int speed = 75;
	private Boolean casterForward = null;
	
	public SerialControlledTank(String serialAddress) throws SerialPortException {
		this.serialAddress = serialAddress;
		initSerialPort();
		resetSerialPort();
	
		System.out.println("Waiting for Arduino");
		blockUntilReady();
		System.out.println("Done with that, let's go!");
	}
	
	protected void initSerialPort() throws SerialPortException {
		System.out.println("Initiating serial connection");
		serialPort = new SerialPort(serialAddress);
		serialPort.openPort();	
		serialPort.setParams(SerialPort.BAUDRATE_9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
	}
	
	protected void resetSerialPort() throws SerialPortException {
		System.out.println("Resetting serial port");
		serialPort.closePort();
		initSerialPort();
	}
	
	private String getResponse() throws SerialPortException {
		char c;
		StringBuilder sb = new StringBuilder();
		while((c = (char) serialPort.readBytes(1)[0]) != '!') {
			System.out.print(c); //FIXME:Minlog
		}
		while(true) {
			c = (char) serialPort.readBytes(1)[0];
			if(c == '\n')
				return sb.toString();
			if(c == '\r')
				continue;
			sb.append(c);
		}
	}
	
	private void blockUntilReady() {
		try {
			String response = getResponse();
			while (!response.equals("ready")) {
				response = getResponse();
			}
		} catch (SerialPortException e) {
			System.out
					.println("SerialPortException waiting for ready response: "
							+ e.getMessage());
		}
	}

	private void sendCommand(String command) {
		try {
			serialPort.writeString(command);
			blockUntilReady();
		} catch (SerialPortException e) {
			System.out.println("Serial write failed: " + e);
		}
	}
	
	private void move(int units, boolean forward) {
		setCaster(true);
		
		String command = (forward) ? "FORWARD" : "BACKWARD";
		sendCommand(command  + "," + units + "\n");
	}
	
	private void turn(int units, boolean left) {
		setCaster(false);
		
		String direction = left ? "LEFT" : "RIGHT";
		sendCommand("turn," + units + "," + direction);
	}
	
	public void setCaster(boolean front) {
		if(front) {
			if (casterForward == null || !casterForward) {
				casterForward = true;
				sendCommand("setCaster,78");
			}
		} else {
			if (casterForward == null || casterForward) {
				casterForward = false;
				sendCommand("setCaster,155");
			}
		}
	}
	
	@Override
	public void moveForward(int units) {
		move(units, true);
	}

	@Override
	public void moveBackward(int units) {
		int oldSpeed = speed;
		setSpeed((int) (speed * 1.1));
		move(units, false);
		setSpeed(oldSpeed);
	}

	@Override
	public void turnRight(int units) {
		turn(units, false);
	}

	@Override
	public void turnLeft(int units) {
		turn(units, true);
	}

	//Average response time halved in microseconds
	@Override
	public double getSensorReading(SensorDirection sensor) {
		String reading = null;
		
		try {
			serialPort.writeString("sensorRead," + sensor.getUsefulName()+","+SAMPLE_SIZE);
			reading = getResponse();
			blockUntilReady();
		} catch (SerialPortException e) {
			System.out.println("Serial write failed: " + e);
		}
		
		if (reading == null) {
			return -1;
		} else {
			return Double.parseDouble(reading); // TODO: This can fail
		}
	}

	@Override
	public void setSensorAngles(int angle) {
		sendCommand("setSensors," + angle);
	}

	@Override
	public void setSpeed(int speed) {
		if (speed == this.speed) {
			return;
		}
		
		this.speed = speed;
		sendCommand("change,motorspeed," + speed);
	}

	public void changeFactor(String factor, float value) {
		sendCommand("change," + factor + "," + value);
		System.out.println("change," + factor + "," + value);
	}
	
	public void uploadConfiguration(TankConfiguration config) {
		setSpeed(config.motorSpeed);
		changeFactor(RIGHT_SPEED_FACTOR, config.rightMotorFactor);
		changeFactor(LEFT_SPEED_FACTOR, config.leftMotorFactor);
		changeFactor(RIGHT_MOTOR_TURNING_LEFT, config.rtlMotorFactor);
		changeFactor(RIGHT_MOTOR_TURNING_RIGHT, config.rtrMotorFactor);
		changeFactor(LEFT_MOTOR_TURNING_LEFT, config.ltlMotorFactor);
		changeFactor(LEFT_MOTOR_TURNING_RIGHT, config.ltrMotorFactor);
	}
	
	public void closeConnection() throws SerialPortException {
		serialPort.closePort();
	}
	
}
