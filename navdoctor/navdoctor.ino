#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_PWMServoDriver.h"
#include <Servo.h>
#include <string>

#define CASTER_SERVO_PIN 10
#define SENSOR_SERVO_PIN 9

#define FWD_UNIT_TIME 1
#define TURN_UNIT_TIME 1

//Sensors
#define LEFT_TRIGGER_PIN  13
#define LEFT_ECHO_PIN  2
#define RIGHT_TRIGGER_PIN 5
#define RIGHT_ECHO_PIN 6
#define FRONT_TRIGGER_PIN 16
#define FRONT_ECHO_PIN 17
#define BACK_TRIGGER_PIN  14
#define BACK_ECHO_PIN  15

//Commands
#define READY  "ready"
#define FWD    "forward"
#define BWD    "backward"
#define TRN    "turn"
#define SETC   "setCaster"
#define SETS   "setSensors"
#define CHG    "change"
#define LF     "leftSpeedFactor"
#define RF     "rightSpeedFactor"
#define LMTL   "leftMotorTurningLeft"
#define LMTR   "leftMotorTurningRight"
#define RMTL   "rightMotorTurningLeft"
#define RMTR   "rightMotorTurningRight"
#define MS     "motorSpeed"
#define SNR    "sensorRead"
#define LE     "left"
#define RI     "right"
#define FT     "front"
#define BK     "back"

//Constants
#define BANG  "!"

Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
Adafruit_DCMotor *leftMotor, *rightMotor;
Servo casterServo, sensorServo;

//Error correction factors
float leftMotorSpeedFactor, rightMotorSpeedFactor;
float leftMotorTurningLeftFactor, rightMotorTurningLeftFactor,
leftMotorTurningRightFactor, rightMotorTurningRightFactor;
int motorSpeed;

String getNthOperand(String command, int n) {
  int count = 0;
  int indexOf;
  String curString = command;
  while(((indexOf = curString.indexOf(",")) != -1) && count < n) {
   count++;
   curString = curString.substring(indexOf+1);
  }
  
  if(count != n) {
   return "";
  }
  
  indexOf = curString.indexOf(",");
  if(indexOf != -1) {
   curString = curString.substring(0, indexOf); 
  }
  
  return curString;
}

void setupSensor(int triggerPin, int echoPin) {
  pinMode(triggerPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void setup() {
  Serial.begin(9600);
  sendMessage(READY);
  
  AFMS.begin();  

  leftMotor = AFMS.getMotor(1);
  rightMotor = AFMS.getMotor(2);
  
  setupSensor(LEFT_TRIGGER_PIN, LEFT_ECHO_PIN);
  setupSensor(RIGHT_TRIGGER_PIN, RIGHT_ECHO_PIN);
  setupSensor(FRONT_TRIGGER_PIN, FRONT_ECHO_PIN);
  setupSensor(BACK_TRIGGER_PIN, BACK_ECHO_PIN);
  
  //defaults
  leftMotorSpeedFactor  = 1;
  rightMotorSpeedFactor = 1;
  motorSpeed = 75;
  leftMotorTurningLeftFactor = 1;
  leftMotorTurningRightFactor = 1;
  rightMotorTurningLeftFactor = 1;
  rightMotorTurningRightFactor = 1;
  
  casterServo.attach(CASTER_SERVO_PIN);
  sensorServo.attach(SENSOR_SERVO_PIN);
}

void moveDir(int units, int dir) {
  int delay_time_ms = units * FWD_UNIT_TIME; 
  
  leftMotor->setSpeed(leftMotorSpeedFactor   * motorSpeed);
  rightMotor->setSpeed(rightMotorSpeedFactor * motorSpeed);
  
  leftMotor->run(dir);
  rightMotor->run(dir);
  
  delay(delay_time_ms);
  
  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);
}

void turnRight(int units) {
  int delayTimeMs = units * TURN_UNIT_TIME;
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  leftMotor->setSpeed(leftMotorTurningRightFactor * motorSpeed);
  rightMotor->setSpeed(rightMotorTurningRightFactor * motorSpeed);
  delay(delayTimeMs);
  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);
}

void turnLeft(int units) {
  int delayTimeMs = units * TURN_UNIT_TIME;
  leftMotor->run(BACKWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(leftMotorTurningLeftFactor * motorSpeed);
  rightMotor->setSpeed(rightMotorTurningLeftFactor * motorSpeed);
  delay(delayTimeMs);
  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);
}

void moveForward(int units) {
 moveDir(units, FORWARD); 
}

void moveBackward(int units) {
  moveDir(units, BACKWARD); 
}

void setServo(Servo s, int degree) {
  s.write(map(degree, 0, 180, 0, 180)); 
}

long getSensorReading(int trigPin, int echoPin){
       digitalWrite(trigPin, LOW);
       delayMicroseconds(2);
       digitalWrite(trigPin, HIGH);
       delayMicroseconds(2);
       digitalWrite(trigPin, LOW);
       return pulseIn(echoPin, HIGH);
}

void sendMessage(String message) {
  Serial.println(BANG + message);  
}

/* The current model assumes that commands can be executed synchronously, if this isn't
   the case, we'll have to re-think this code */
void parseCommand(String command) {
  String instruction;
  String rawOperand;  
  int operand;
  
  instruction = getNthOperand(command, 0);
  rawOperand = getNthOperand(command, 1);
  operand = rawOperand.toInt();
  
  if (instruction.equalsIgnoreCase(FWD)){
     moveForward(operand);
  }
  
  if (instruction.equalsIgnoreCase(BWD)){
     moveBackward(operand); 
  }
  
  if (instruction.equalsIgnoreCase(TRN)){
     String dir = getNthOperand(command,2);
     if(dir.equalsIgnoreCase(LE)) {
      turnLeft(operand);
     }
     else if(dir.equalsIgnoreCase(RI)) {
      turnRight(operand);
     } 
  }
  
  if (instruction.equalsIgnoreCase(SETC)){
     setServo(casterServo, operand);
  }
  
  if (instruction.equalsIgnoreCase(SETS)){
     setServo(sensorServo, operand); 
  }
  
  if (instruction.equalsIgnoreCase(CHG)){
    float value = getNthOperand(command,2).toFloat();
    if(rawOperand.equalsIgnoreCase(LF)) {
      leftMotorSpeedFactor = value;
      Serial.println(value);
    }
    else if(rawOperand.equalsIgnoreCase(RF)) {
      rightMotorSpeedFactor = value; 
    }
    else if(rawOperand.equalsIgnoreCase(MS)) {
      motorSpeed = value; 
    }
    else if(rawOperand.equalsIgnoreCase(LMTL)) {
      leftMotorTurningLeftFactor = value; 
    }
    else if(rawOperand.equalsIgnoreCase(LMTR)) {
      leftMotorTurningRightFactor = value; 
    }
    else if(rawOperand.equalsIgnoreCase(RMTL)) {
      rightMotorTurningLeftFactor = value;
    }
    else if(rawOperand.equalsIgnoreCase(RMTR)) {
      rightMotorTurningRightFactor = value; 
    }
  }
  if (instruction.equalsIgnoreCase(SNR)){
     float total = 0;
     int count = getNthOperand(command,2).toInt();
     
     for (int i = 0; i < count; i++) {
       long reading;
       if(rawOperand.equalsIgnoreCase(LE)) {
         reading = getSensorReading(LEFT_TRIGGER_PIN, LEFT_ECHO_PIN);
       }
       else if(rawOperand.equalsIgnoreCase(RI)) {
         reading = getSensorReading(RIGHT_TRIGGER_PIN, RIGHT_ECHO_PIN);
       }
       else if(rawOperand.equalsIgnoreCase(FT)) {
         reading = getSensorReading(FRONT_TRIGGER_PIN, FRONT_ECHO_PIN); 
       }
       else if(rawOperand.equalsIgnoreCase(BK)) {
         reading = getSensorReading(BACK_TRIGGER_PIN, BACK_ECHO_PIN);
       }
       total = total + (reading / 2);
     }
     
     sendMessage(String(total / count));
  }

}

/* Event loop is: read command, parse command, respond ready. It is expected that
   the other end will not attempt to push more commands down the pipe. */
void loop() {
  String input;
  
  if(Serial.available() > 0) {
    input = Serial.readString();
    parseCommand(input);
    sendMessage(READY);
  }
}
