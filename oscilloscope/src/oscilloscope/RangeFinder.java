package oscilloscope;

import java.io.IOException;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class RangeFinder implements AutoCloseable {
	
	public static void main(String[] args) {
		//RangeFinder.printAvailablePorts();
		int count = 0;
		int limit = 100;
		try (RangeFinder rangeFinder = new RangeFinder("COM4")) {
			rangeFinder.startCommand();
			while (count++ < limit) {
				long range = rangeFinder.findRange();
				System.out.println("Range: " + range);
			}
			rangeFinder.stopCommand();
		} catch (SerialPortException e) {
			System.out.println("Serial port error: " + e.getMessage());
		} catch (Exception e) {
			System.out.println("Unspecified error: " + e.getMessage());
		}
	}
	
	private SerialPort serialPort;
	
	public RangeFinder(String comPort) throws SerialPortException {
		serialPort = new SerialPort(comPort);
		serialPort.openPort();	
		serialPort.setParams(SerialPort.BAUDRATE_9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
		
		/* Opening the arduino serial port connection resets the arduino, wait 5 seconds */
		System.out.println("Waiting for Arduino");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException ignored) {}
	}
	
	public static void printAvailablePorts() {
        String[] portNames = SerialPortList.getPortNames();
        
        if (portNames.length == 0) {
        	System.out.println("No serial ports found");
        	return;
        }
        
        for(int i = 0; i < portNames.length; i++){
            System.out.println(portNames[i]);
        }
	}
	
	public long findRange() {
		try {
			long duration = scan();
			return duration;
		} catch (SerialPortException e) {
			return -1;
		}
	}
	
	private void clearBuffer() throws SerialPortException {
		int i;
		while((i = serialPort.getInputBufferBytesCount()) > 0) {
			serialPort.readBytes(i);
		}
	}
	
	private long scan() throws SerialPortException {
		clearBuffer();
		StringBuilder sb = new StringBuilder();
		while(sb.length() < 100) {		
			String scan = serialPort.readString();
			
			if(scan != null && !scan.isEmpty()) {
				sb.append(scan);
			}
		}
		String[] rawScans = sb.toString().split("_");	
		long[] scans = new long[rawScans.length-2];
		
		for(int i = 1; i < rawScans.length-1; i++) {
			try {
				scans[i-1] = Long.parseLong(rawScans[i]);
			} catch (NumberFormatException e) {
				continue;
			}
		}
		
		long avg = scans[0];
		for(long scan : scans) {		
			avg += scan;
			avg /= 2;
		}
		return avg;
	}

	@Override
	public void close() throws Exception {
		if (!serialPort.closePort())
			throw new IOException("Closing serial port failed");
	}

	public void startCommand() throws SerialPortException {
		if (!serialPort.writeString("START\0")) {
			System.out.println("START failed");
			return;
		}

		System.out.println("START sent");
	}

	public void stopCommand() throws SerialPortException {
		serialPort.writeString("STOP\0");
		System.out.println("STOP sent");
	}
		
}
