
#define trigPin 13
#define echoPin 12

unsigned long lastCmdCheck = 0;
boolean doSensorRead = false;

long getSensorReading() {
 digitalWrite(trigPin, LOW);
 delayMicroseconds(2);
 digitalWrite(trigPin, HIGH);
 delayMicroseconds(2);
 digitalWrite(trigPin, LOW);
 return pulseIn(echoPin, HIGH);
}

void checkForCommands() {
 if (Serial.available() > 0) {
  String command = Serial.readString();
  
  if (command.startsWith("START")) {
    doSensorRead = true;
  } else if (command.startsWith("STOP")) {
    doSensorRead = false;
  }
 } 
}

void loop() {
  unsigned long now = millis();
  if ((now - lastCmdCheck) > 1000) {
    lastCmdCheck = now;
    checkForCommands();
  }
  
  if (doSensorRead) {
   long duration = getSensorReading();
   Serial.print(duration);
   Serial.print("_");
  }
}

void setup() {
 Serial.begin(9600);
 pinMode(trigPin, OUTPUT);
 pinMode(echoPin, INPUT); 
}
