/* 
 This is a test sketch for the Adafruit assembled Motor Shield for Arduino v2
 It won't work with v1.x motor shields! Only for the v2's with built in PWM
 control
 
 For use with the Adafruit Motor Shield v2 
 ---->	http://www.adafruit.com/products/1438
 
 This sketch creates a fun motor party on your desk *whiirrr*
 Connect a DC motor to M1
 Connect a hobby servo to SERVO1
 */

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_PWMServoDriver.h"
#include <Servo.h>

// me --  
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <string>
#include <cstring>

// Front Sensor
#define trigPinFront 13
#define echoPinFront 12

// Back Sensor
#define trigPinBack 7
#define echoPinBack 6

// Left Sensor
#define trigPinLeft 11
#define echoPinLeft 10

// Right Sensor
#define trigPinRight 9
#define echoPinRight 8

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// And connect a DC motor to port M1
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

// We'll also test out the built in Arduino Servo library
Servo frontServoMotor, backServoMotor;

boolean doSensorRead = false;
unsigned long lastCmdCheck = 0;

// Sensor On
boolean frontSensorRunning = false;
boolean backSensorRunning = false; // -working on-
boolean leftSensorRunning = false; // -working on-
boolean rightSensorRunning = false; // -working on-

//Serial write
String serialWriteString = "";


void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("MMMMotor party!");

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz

  // Attach a servo to pin #10
  frontServoMotor.attach(10);
  backServoMotor.attach(9);
  frontServoMovement(150);
  backServoMovement(150);
  
  // Front sensor setup
  pinMode(trigPinFront, OUTPUT);
  pinMode(echoPinFront, INPUT); 
  
  // Front sensor setup
  pinMode(trigPinBack, OUTPUT);
  pinMode(echoPinBack, INPUT); 
  
  // Front sensor setup
  pinMode(trigPinLeft, OUTPUT);
  pinMode(echoPinLeft, INPUT); 
  
  // Front sensor setup
  pinMode(trigPinRight, OUTPUT);
  pinMode(echoPinRight, INPUT); 

}

void parseCommand(String in) {
String part1;
String part2;

part1 = in.substring(0, in.indexOf(" "));
part2 = in.substring(in.indexOf(" ") + 1);


  if(part1.equalsIgnoreCase("forward")) {
    moveForward();
  }
  if(part1.equalsIgnoreCase("backward")) {
    moveBackward();
  }
  if(part1.equalsIgnoreCase("stop")) {
    moveStop();
  }
  if(part1.equalsIgnoreCase("left")) {
    rotateLeft();
  }
  if(part1.equalsIgnoreCase("right")) {
    rotateRight();
  }
  // Front Servo Movement
  if(part1.equalsIgnoreCase("front")) {
    frontServoMovement(part2.toInt());
  }
  // Scanner Front Commands
  if (part1.equalsIgnoreCase("frontScannerStart")) {
    frontSensorRunning = true;
  }
  if(part1.equalsIgnoreCase("frontScannerStop")) {
    frontSensorRunning = false;
  }
  
  // Back Servo Movement
  if(part1.equalsIgnoreCase("back")) {
    backServoMovement(part2.toInt());
  }
  // Scanner Back Commands
  if (part1.equalsIgnoreCase("backScannerStart")) {
    backSensorRunning = true;
  }
  if(part1.equalsIgnoreCase("backScannerStop")) {
    backSensorRunning = false;
  }
  // Scanner Left Commands
  if (part1.equalsIgnoreCase("leftScannerStart")) {
    leftSensorRunning = true;
  }
  if(part1.equalsIgnoreCase("leftScannerStop")) {
    leftSensorRunning = false;
  }
  // Scanner Right Commands
  if (part1.equalsIgnoreCase("rightScannerStart")) {
    rightSensorRunning = true;
  }
  if(part1.equalsIgnoreCase("rightScannerStop")) {
    rightSensorRunning = false;
  }
  
  if(part1.equalsIgnoreCase("scan")) {
    if(part2 == "on"){
      scanMode(true);
    }
  }
}

//75 is a good value for battery powered
String input;
char c;
void loop() {
  
  // Check for input
  if(Serial.available() > 0) {
    input = Serial.readString();
    parseCommand(input);
  }
  
  // Run the scanners
  scan();
  
  // outPutTosensor
  outputSerialString();
}



void moveForward() {
    leftMotor->run(FORWARD);
    leftMotor->setSpeed(75);
    rightMotor->run(FORWARD);
    rightMotor->setSpeed(75);  
}

void moveBackward() {
    leftMotor->run(BACKWARD);
    leftMotor->setSpeed(75);
    rightMotor->run(BACKWARD);
    rightMotor->setSpeed(75);     
}

void moveStop() {
    leftMotor->setSpeed(0);
    rightMotor->setSpeed(0);
    frontServoMovement(150);
    backServoMovement(150);
}

int i;

void rotateLeft() {
for (i=100; i<150; i++) {
  rightMotor->run(FORWARD);  
  rightMotor->setSpeed(i);
  leftMotor->run(BACKWARD);  
  leftMotor->setSpeed(i);
  delay(20);
}
  rightMotor->run(FORWARD);  
  rightMotor->setSpeed(0);
  leftMotor->run(BACKWARD);  
  leftMotor->setSpeed(0);
}

void rotateRight() {
for (i=100; i<150; i++) {
  leftMotor->run(FORWARD);  
  leftMotor->setSpeed(i);
  rightMotor->run(BACKWARD);  
  rightMotor->setSpeed(i);
  delay(20);
}
  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);
}

void scanMode(boolean scanMode) {
       for (i=100; i<180; i++) {
        frontServoMotor.write(map(i, 0, 180, 0, 180));
        backServoMotor.write(map(i, 0, 180, 0, 180));
        delay(128);
     }
     
      for (i=180; i!=100; i--) {
        frontServoMotor.write(map(i, 0, 180, 0, 180));
        backServoMotor.write(map(i, 0, 180, 0, 180));
        delay(128);
     }
}



// Front Sensors
long frontSensorReading() {
 digitalWrite(trigPinFront, LOW);
 delayMicroseconds(2);
 digitalWrite(trigPinFront, HIGH);
 delayMicroseconds(2);
 digitalWrite(trigPinFront, LOW);
 return pulseIn(echoPinFront, HIGH);
}

// Front Servo Movement
void frontServoMovement(int degree) {
   frontServoMotor.write(map(degree, 0, 180, 0, 180));
}

// Back Sensors
long backSensorReading() {
 digitalWrite(trigPinBack, LOW);
 delayMicroseconds(2);
 digitalWrite(trigPinBack, HIGH);
 delayMicroseconds(2);
 digitalWrite(trigPinBack, LOW);
 return pulseIn(echoPinBack, HIGH);
}

// Back Servo Movement
void backServoMovement(int degree) {
   backServoMotor.write(map(degree, 0, 180, 0, 180));
}

// Left Sensors
long leftSensorReading() {
 digitalWrite(trigPinLeft, LOW);
 delayMicroseconds(2);
 digitalWrite(trigPinLeft, HIGH);
 delayMicroseconds(2);
 digitalWrite(trigPinLeft, LOW);
 return pulseIn(echoPinLeft, HIGH);
}

// Right Sensors
long rightSensorReading() {
 digitalWrite(trigPinRight, LOW);
 delayMicroseconds(2);
 digitalWrite(trigPinRight, HIGH);
 delayMicroseconds(2);
 digitalWrite(trigPinRight, LOW);
 return pulseIn(echoPinRight, HIGH);
}



// Scan funtion 
void scan(){ 
    // Is the front sensor on this loop?
    if(frontSensorRunning){
      long durationFront = frontSensorReading();
      String s = (String) durationFront;
      writeToSerial("SensorFront:"+s);
    }
    // Is the back sensor on this loop?
    if(backSensorRunning){
      long durationBack = backSensorReading();
      String s = (String) durationBack;
      writeToSerial("SensorBack:"+s);
    }
    // Is the left sensor on this loop?
    if(leftSensorRunning){
      long durationLeft = leftSensorReading();
      String s = (String) durationLeft;
      writeToSerial("SensorLeft:"+s);
    }
    // Is the right sensor on this loop?
    if(rightSensorRunning){
      long durationRight = backSensorReading();
      String s = (String) durationRight;
      writeToSerial("SensorRight:"+s);
    }
}

// Write to serial
void outputSerialString(){
  if(serialWriteString.length()>0){
    
    String toSend = "START;";
    toSend = toSend+serialWriteString+"END;";
    
    Serial.println(toSend);
    serialWriteString = "";
  }  
}

// Use this for Writeing to output
void writeToSerial(String input){
  serialWriteString = serialWriteString + input + ";";
}


