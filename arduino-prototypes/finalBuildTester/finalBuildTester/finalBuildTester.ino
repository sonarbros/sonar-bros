/* 
 This is a test sketch for the Adafruit assembled Motor Shield for Arduino v2
 It won't work with v1.x motor shields! Only for the v2's with built in PWM
 control
 
 For use with the Adafruit Motor Shield v2 
 ---->	http://www.adafruit.com/products/1438
 
 This sketch creates a fun motor party on your desk *whiirrr*
 Connect a DC motor to M1
 Connect a hobby servo to SERVO1
 */

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_PWMServoDriver.h"
#include <Servo.h>

// SENSORS
//LEFT
#define leftTrigPin 13
#define leftEchoPin 2
//RIGHT
#define rightTrigPin 5
#define rightEchoPin 6
//FRONT
#define frontTrigPin 16
#define frontEchoPin 17
//BACK
#define backTrigPin 14
#define backEchoPin 15

//
#define motorSpeed 75
#define LEFT_MOTOR_FACTOR 1.03
#define RIGHT_MOTOR_FACTOR 1.01



// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// And connect a DC motor to port M1
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

// We'll also test out the built in Arduino Servo library
Servo castor, servos;

void setup() {

  
  Serial.begin(9600);           // set up Serial library at 9600 bps

  AFMS.begin();  // create with the default frequency 1.6KHz

  // Attach a servo to pin #10
  castor.attach(10);
  servos.attach(9);

  setServos(150);
  setCastor(73);
  
  
  // sensors
  //LEFT
  pinMode(leftTrigPin, OUTPUT);
  pinMode(leftEchoPin, INPUT);  
  //RIGHT
  pinMode(rightTrigPin, OUTPUT);
  pinMode(rightEchoPin, INPUT); 
  //FRONT
  pinMode(frontTrigPin, OUTPUT);
  pinMode(frontEchoPin, INPUT); 
  //BACK
  pinMode(backTrigPin, OUTPUT);
  pinMode(backEchoPin, INPUT); 
}

//75 is a good value for battery powered
String input;
char c;
void loop() {
  if(Serial.available() > 0) {
    input = Serial.readString();
    Serial.println(input);
    
    parseCommand(input);

       
  }
}


void parseCommand(String in) {
String part1;
String part2;

part1 = in.substring(0, in.indexOf(" "));
part2 = in.substring(in.indexOf(" ") + 1);

// sensors
  if(part1.equalsIgnoreCase("getFrontSensor")) {
   long duration = frontSensorReading();
   Serial.print(duration);
  }
   else if(part1.equalsIgnoreCase("getBackSensor")) {
   long duration = backSensorReading();
   Serial.print(duration);
  }
  else if(part1.equalsIgnoreCase("getLeftSensor")) {
   long duration = leftSensorReading();
   Serial.print(duration);
  }
  else if(part1.equalsIgnoreCase("getRightSensor")) {
   long duration = rightSensorReading();
   Serial.print(duration);
  }

// Servos
  else if(part1.equalsIgnoreCase("setServos")) {
    setServos(part2.toInt());
  }
// Castor steering
  else if(part1.equalsIgnoreCase("setCastor")) {
    setCastor(part2.toInt());
  }

//Movment
   else if(part1.equalsIgnoreCase("forward")) {
    moveForward();
    delay(1000);
    moveStop();
  }
  if(part1.equalsIgnoreCase("backward")) {
    moveBackward();
    delay(1000);
    moveStop();
  }
  if(part1.equalsIgnoreCase("stop")) {
    moveStop();
  }
  
// Rotaion
  if(part1.equalsIgnoreCase("left")) {
    rotateLeft();
  }
  if(part1.equalsIgnoreCase("right")) {
    rotateRight();
  }
  
  if(part1.equalsIgnoreCase("eleft")) {
    elliotRotateLeft(); 
  }
  
  if(part1.equalsIgnoreCase("eright")) {
    elliotRotateRight(); 
  }
  
}


// movement
void moveForward() {
    leftMotor->run(FORWARD);
    leftMotor->setSpeed(LEFT_MOTOR_FACTOR * motorSpeed);
    rightMotor->run(FORWARD);
    rightMotor->setSpeed(RIGHT_MOTOR_FACTOR * motorSpeed);  
}

void moveBackward() {
    leftMotor->run(BACKWARD);
    leftMotor->setSpeed(LEFT_MOTOR_FACTOR * motorSpeed);
    rightMotor->run(BACKWARD);
    rightMotor->setSpeed(RIGHT_MOTOR_FACTOR * motorSpeed);     
}

void moveStop() {
    leftMotor->setSpeed(0);
    rightMotor->setSpeed(0);
}

int i;

void elliotRotateLeft() {
  rightMotor->run(FORWARD);
  rightMotor->setSpeed(150);
  leftMotor->run(BACKWARD);
  leftMotor->setSpeed(150);
  delay(1000);
  rightMotor->setSpeed(0);
  leftMotor->setSpeed(0);  
}

void elliotRotateRight() {
  rightMotor->run(BACKWARD);
  rightMotor->setSpeed(150);
  leftMotor->run(FORWARD);
  leftMotor->setSpeed(150);
  delay(1000);
  rightMotor->setSpeed(0);
  leftMotor->setSpeed(0);  
}
// rotation
void rotateLeft() {
for (i=100; i<150; i++) {
  rightMotor->run(FORWARD);  
  rightMotor->setSpeed(i);
  leftMotor->run(BACKWARD);  
  leftMotor->setSpeed(i);
  delay(20);
}
  rightMotor->run(FORWARD);  
  rightMotor->setSpeed(0);
  leftMotor->run(BACKWARD);  
  leftMotor->setSpeed(0);
}

void rotateRight() {
for (i=100; i<150; i++) {
  leftMotor->run(FORWARD);  
  leftMotor->setSpeed(i);
  rightMotor->run(BACKWARD);  
  rightMotor->setSpeed(i);
  delay(20);
}
  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);
}



// sensors
long leftSensorReading(){
     return getSensorReading(leftTrigPin, leftEchoPin);
}

long rightSensorReading(){
  return getSensorReading(rightTrigPin, rightEchoPin);
}

long frontSensorReading(){
  return getSensorReading(frontTrigPin, frontEchoPin);
}

long backSensorReading(){
  return getSensorReading(backTrigPin, backEchoPin);
}

long getSensorReading(int trigPinIn, int echoPinIn){
       digitalWrite(trigPinIn, LOW);
       delayMicroseconds(2);
       digitalWrite(trigPinIn, HIGH);
       delayMicroseconds(2);
       digitalWrite(trigPinIn, LOW);
       return pulseIn(echoPinIn, HIGH);
}


// Servos
void setServos(int degree) {
   servos.write(map(degree, 0, 180, 0, 180));
}

// Castor steeeing
void setCastor(int degree) {
   castor.write(map(degree, 0, 180, 0, 180));
}

