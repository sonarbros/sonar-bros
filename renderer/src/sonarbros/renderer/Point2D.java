package sonarbros.renderer;


public class Point2D {

	public Double x;
	public Double y;
	
	public Point2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
}
