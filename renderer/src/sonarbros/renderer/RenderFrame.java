package sonarbros.renderer;

import javax.swing.JFrame;

import sonarbros.renderer.listener.MapSocketListener;

public class RenderFrame extends JFrame {

	private static final long serialVersionUID = -5048172762773340186L;
	private MapPointCloud cloud;
	private MapSocketListener listener;
	
	public RenderFrame() {
		super("Satank Map Renderer");
		cloud = new MapPointCloud();
		listener = new MapSocketListener(cloud);
		new Thread(listener).start();
		
		add(new MapRenderCanvas(cloud));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
}
