package sonarbros.renderer;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.Timer;

public class MapRenderCanvas extends JPanel implements ActionListener {

	private static final long serialVersionUID = 7553140596122794809L;
	private MapPointCloud cloud;
	private Timer redrawTimer;
	
	public MapRenderCanvas(MapPointCloud cloud) {
		this.cloud = cloud;
		redrawTimer = new Timer(1000, this);
		redrawTimer.start();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		Set<Point2D> points = cloud.getPoints();
		
		if (points.size() == 0) {
			return;
		}
		
		// TODO: Calcualte mult{X,Y} for when values are greater than window values etc.
		float multX = 22f;
		float multY = 22f;
		
		g.translate(getWidth() / 2, getHeight() / 2);
		for (Point2D p : points) {			
			g.drawOval((int)(p.y * multY), (int)(p.x * multX), 1, 1);
		}
		g.translate(-(getWidth() / 2), -(getHeight() / 2));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		paintComponent(getGraphics());
	}
	
}
