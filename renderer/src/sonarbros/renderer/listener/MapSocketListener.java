package sonarbros.renderer.listener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import sonarbros.renderer.MapPointCloud;
import sonarbros.renderer.Point2D;

public class MapSocketListener implements Runnable {

	private static final int PORT = 60833;
	private MapPointCloud cloud;
	
	public MapSocketListener(MapPointCloud cloud) {
		this.cloud = cloud;
	}

	@Override
	public void run() {
		try (ServerSocket sock = new ServerSocket(PORT)) {
			while (true) {
				try (Socket client = sock.accept()) {
					handleClient(client);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void handleClient(Socket client) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
		String line;
		
		while ((line = in.readLine()) != null) {
			if (line.length() > 0) {
				if (line.contains(",")) {
					String[] chunks = line.split(",");
					double x = Double.parseDouble(chunks[0]);
					double y = Double.parseDouble(chunks[1]);
					
					System.out.println(x);
					System.out.println(y);
					
					cloud.addPoint(new Point2D(x, y));
				}
			} else {
				break;
			}
		}
	}
	
}
