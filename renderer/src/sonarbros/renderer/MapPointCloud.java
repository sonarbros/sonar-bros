package sonarbros.renderer;

import java.util.HashSet;
import java.util.Set;

public class MapPointCloud {

	private HashSet<Point2D> points;
	
	public MapPointCloud() {
		points = new HashSet<>();
	}
	 
	public void addPoint(Point2D point2d) {
		synchronized (points) {
			points.add(point2d);
		}
	}
	
	@SuppressWarnings("unchecked") // Ignore this
	public Set<Point2D> getPoints() {
		synchronized (points) {
			return (Set<Point2D>) points.clone();
		}
	}
	
}
