\section{Prototypes}
Following the initial design and architecture discussions, the group gathered to produce a range of prototypes to ensure that the chosen components met the requirements for the gadget. At first, it was decided that hardware prototypes would be carried out in-order to ensure each component behaved as expected as, if they did not, the potential errors which could arise would result in a large refactoring process which would be infeasible due to the time constraints present during the project.

\subsection{Components Utilised}
The following components were tested so that their purpose could be measured effectively to see if they met the requirements, for their given task, for the final tank build.
\begin {itemize}
\item{\textbf{HC-SR04 Sonar Sensor} - }
    As to why the HC-SR04 sonar sensor was chosen, please refer to the Components Chosen section of this document which extensively details the specifications and reasons as to why this component was beneficial to the build.
\item{\textbf{6v-12v Micro DC Motor} - }\footnote{http://hobbycomponents.com/motors-servos/345-130-motor-eco-friendly-6v-12v-micro-dc-motor}
    This motor was chosen as the voltage range, 6v to 12v, the high rpm of 7000, and the small dimensions, provided a strong platform with which the motor prototype could be tested. This particular brand of DC motor is common among electrical hobbyists and the Arduino platform provides a large swathe of functionality and assistance when using them. Due to this, this motor was chosen and details of how it was used in the prototype will be discussed in the following motor prototype section of this chapter.
\item{\textbf{Adafruit Motor Shield} - }\footnote{http://www.adafruit.com/products/1438}
    \begin{figure}[H]
        \centering
        \includegraphics[width=0.5\textwidth]{img/ada}
        \caption{The Adafruit Motor shield for Arduino}
    \end{figure}
    The Adafruit Motor Shield for Arduino was included in the prototype stages of development due to the fact is enabled the group to control multiple motors and servos concurrently without the requirement to implement a complex H-bridge circuit thus removing a large amount of complexity. For further details on the technical aspects of this product, please refer to the footnote to the product website.
\item{\textbf{Vktech MG90S Metal Geared Micro Servo} - }
    This particular servo was chosen due to its low cost, small size, and appropriate torque. Each of these aspects met the requirements the group had for these components. Further details on their performance during the prototypes will be detailed in the servo prototype section of this chapter. The full technical specifications of this components, relevant to the project, are as follows\footnote{http://www.amazon.co.uk/Vktech-MG90S-Geared-Helicopter-Airplane/dp/B00FF26480}: \\

    \begin{tabular}{ll}
        \textbf{Working Torque:} & 2KG/cm \\
        \textbf{Reaction Speed:} & 0.11 seconds / 60 degrees (4.8v) \\
        \textbf{Rotation Angle:} & 180 degrees \\
        \textbf{Product Size:} & Approx. 23 x 12 x 29cm \\
    \end{tabular}

\end{itemize}
\subsection{Sonar Prototype}
As the tanks mapping functionality is sonar based, it was extremely important to establish the limitations of the HC-SR07 sonar sensor as well as an effective method to drive each sensor in order to acquire useful results. Following, is the test rig constructed to measure the performance of the sonar sensors at two different distances, 15cm and 30cm:
\begin{figure}[width=1\textwidth]
    \centering
    \includegraphics[width=0.5\textwidth]{img/sonar_side}
    \caption{Side view of sonar sensor prototype}
\end{figure}
\begin{figure}[width=1\textwidth]
    \centering
    \includegraphics[width=0.5\textwidth]{img/sonar_top}
    \caption{Top view of sonar sensor prototype}
\end{figure}
\clearpage
\begin{figure}[width=1\textwidth]
    \centering
    \includegraphics[width=0.5\textwidth]{img/sonar_schem}
    \caption{Schematic of the sonar sensor prototype}
\end{figure}

To drive the sonar sensor, code and wiring was used from an instructable\footnote{http://www.instructables.com/id/Easy-ultrasonic-4-pin-sensor-monitoring-hc-sr04/} acquired from the web. To facilitate value capture, the code was modified to talk over a serial port with a Java program; this reduced development time later on as the Raspberry Pi would run the same Java code.
The Java program utilised was the jSSC\footnote{https://github.com/scream3r/java-simple-serial-connector} library which enabled the Raspberry Pi to talk to the Arduino over a virtual COM port.
For several distances, 2mm apart 10000 samples were captures from the sensor. These samples were stored to a file and later analysed using GNU Octave and later found to be appropriate for the purposes of mapping. \\

However, there was a limitation discovered during this prototype with the sensor in that the initial plan to construct a forward "arm" on to the tank with multiple sensors would be infeasible due to interference present from the other sensors present on the forward arm. Therefore, to combat this, it was decided a simpler model of a sonar sensor to each side would be adopted instead. \\*\\*

\subsection{DC Motor Prototype}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{img/motor_schematic}
    \caption{Stepper motor prototype schematic}
\end{figure}

\hyperref[https://www.youtube.com/watch?v=RjbZQpn7tdw]{DC motors attached to the gearbox video} \\

Initially, stepper motors were chosen to investigate if those were powerful enough to drive the tank. However, it was quickly discovered that they were no where near powerful enough to fulfill this requirement and were subsequently abandoned in favour of the DC motors. \\

Further investigation into the chosen project, the tank, uncovered potential issues with the motors to power movement of the tank. The issues which arose were:
\begin{itemize}
    \item
        Stepper motors were not fast or strong enough to move the tank at a sufficient speed.
    \item
        Driving the motors programatically.
\end{itemize}

The first task was to remedy the situation with the stepper motors as it was of paramount importance to ensure movement of the tank. Following discussions, the following 3V DC motors were purchased: http://www.maplin.co.uk/p/small-motor-yg13p.  \\

Extensive prototypes using these motors uncovered that they were powerful and fast enough to power the tank. However, issues arose when attempts to drive the motors concurrently programatically returned negative results. To resolve this issue and simplify the process of driving the motors, the group invested in the purchase of an Adafruit motor shield for the Arduino. With a fully dedicated PWM driver chip on board as well as 4-H bridges, the Adafruit motor shield will remove the complexity in constructing a H-bridge circuit and allow pulse wave modulation for more accurate control of each motor. The Adafruit also provide an API to programatically control these motors across the bridge reducing the complexity even further and making life that little bit easier for the team. 
There is one downside to using the Adafruit motor shield however and that is it only works with 5V DC motors. This resulted in the purchase of the previously mentioned motors to replace the 3V DC motors purchased previously. \\

Primary movement of the tank will come from two DC motors driving a gearbox\footnote{http://www.amazon.co.uk/Tamiya-70168-Double-Gearbox-Kit/dp/B005PQ9VJK} which, in turn, will power tank treads\footnote{http://www.amazon.co.uk/Tamiya-70108-000-Tracked-Vehicle-Chassis/dp/B002DR3H5S}. The prototype carried out consisted of attaching the two motors to the gearbox then attaching this unit to a basic tank frame with treads. From this, moving forwards, backwards, and basic turning was recorded and measured for speed and accuracy. \\

Post-prototype, it was discovered that the accuracy of turns provided by the tank treads was insufficient for the needs of accurately rendering a map and therefore they were replaced with a castor wheel trike model. In terms of motor power, the motors were found to be sufficiently powerful to drive the tank at a reasonable speed.

\subsection{Servo Prototype}
The servo prototype was a simple manner of checking the torque and rotational abilities of the motors. The prototype carried out consisted of attaching a sonar sensor to a t-shaped harness which was then to a wired servo. From this, rotational speed and accuracy was measured in 5 degree increments for accuracy and 90 degree periods for speed. Following extensive measurements, it was quickly discovered and decided upon that the chosen servo motors are ideal and perfectly meet the requirements of the tank. \\

However, in a subsequent prototype using two servos, it was discovered that driving them both concurrently at the same time was incredibly difficult as well as driving the DC motors for movement. Therefore, the solution to this issue which was provided to the group via the Adafruit motor shield for Arduino, resulted in the group being able to control the servo and dc motors independently through the Arduino.

