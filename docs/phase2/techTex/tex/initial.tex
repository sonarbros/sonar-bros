\section{Initial Design}
\subsection{Initial Ideas}
Potential applications of sonar were discussed, including:

\begin{itemize}
    \item Radar
    \item Autonomous Vehicle
    \item Medical Applications
    \item 3D Scanning
\end{itemize}
Given time constraints, only one initial idea was taken forward to explorative development, the 3D scanner.

This initial idea consisted of using several ultrasonic sensors mounted in a specific manner to scan an object while it was rotating on a mechanical base plate. After capturing the readings with sonar sensors, these readings would be input into an algorithm which would measure the distance from the sensor to the object in-order to provide a 3D representation of the object. The sensor is optimal in a 30 degree range which made it suitable to use in the size of scanner in mind.
\begin{figure}[width=0.45\textwidth]
    \centering
    \includegraphics[width=0.45\textwidth]{img/3d_scan}
    \caption{Example of an existing 3D scanner}
\end{figure}
\begin{figure}[width=0.45\textwidth]
    \centering
    \includegraphics[width=0.45\textwidth]{img/sonar_performance}
    \caption{Sonar Performance Test from Pg 5 - HC-SR04 Users Manual, Cryton Technologies, May 2013}
\end{figure}
\\
During initial prototypes, unfortunately, some problems and limitations were encountered with the sonar readings.

The issue which arose was that readings from the sonar sensors were not consistent at the minimum feature size required, 1mm. In particular, the standard deviation of 10000 scans at one distance turned into as much as 5mm, rendering temporal noise elimination useless. An attempt was made to reduce standard deviation programmatically by eliminating the max and min for the set in a loop but reducing standard deviation to acceptable levels simply left no scans. The resulting model with this level of accuracy, in the worst case, would be have a 1cm zigzag rendered for a flat surface.

With the information obtained from these prototypes, it was clear that the 3D scanner would not be a good candidate for the final idea and was therefore abandoned. An extension of a different initial idea, the autonomous vehicle, was developed and ultimately selected.

\subsection{Design Approach to the Autonomous Vehicle}
Following the decision to build an autonomous vehicle, discussions began on which initial tasks should be carried out first and how findings gathered from the prototyping stage, would factor into the final design. To ensure a clean and consistent design, the four design principles and their potential application to the gadget were carefully considered. Following is a description of how each principle was included and assisted in the initial design of the autonomous vehicle:

\subsubsection{Simplicity Favours Regularity}
This particular design principle encourages a minimalistic and regular approach to the inner logic of the system. How this was incorporated into the tank design then implementation was done through ensuring that the sonar readings, regardless of the surrounding environment, would return a clean and consistent format to the Arduino then Raspberry Pi for transmission. By doing this, it would minimise the potential scope for error and increase the probability of a more accurate map as well as increasing performance. However, due to the nature of the project, a reasonable scope of error must be accounted for and this be dealt with programmatically.
\subsubsection{Smaller is Faster}
"Smaller is Faster" is a design principle that states a system which utilises smaller and fewer pieces of hardware will inevitably be faster than one which uses a higher number. As our tank has a reasonably low power consumption, we are not affected by the exception to this rule which nullifies it in cases where the larger system has a higher power consumption. Smaller is faster was considered and, as will be seen, an example of this was when the number of sonar sensors was reduced in-order to speed up and increase performance of the mapping. Overall, the number of components was kept as small as possible so that this principle could be met.
\subsubsection{Make The Common Case Fast}
Making the common case fast ensures that the primary and most common case / application of the gadget is performed at the best speed possible. With this in mind, "Making the Common Case Fast" with regards to the autonomous vehicle, was implemented via a daemon process listening to a serial socket for data readings specifically from the sensor (on a multi-threaded basis). By prioritising this data ensured that the sonar readings, the most common case, are processed as fast as possible by the Arduino and Raspberry Pi and, as a result, will make the entire system perform to a faster standard.
\subsubsection{Good Design Demands Good Compromise}
Throughout this document, each design change and the reasons behind each will be detailed extensively. Through doing this, the tenant of "Good Design Demands Good Compromise" will be met reflecting on the groups textbook approach to ensure an excellent final product. As Albert Einstein famously said, "We cannot solve our problems with the same thinking we used when creating them".

\subsubsection{First Design Attempt}
With this information, it was decided that the autonomous vehicle will take the form of a tank and utilise a total of 6 sonar sensors. The treads, it was decided, will perform movement accurate enough for navigation but also stability to take the sonar readings. With these sensors, the readings will be taken via an Arduino which will also control the various motors in the tank and, due to its excellent capability in interfacing the outside components, the sonar readings will finally be passed to the Raspberry Pi which will transmit them to a local network computer, via WiFi, to produce a map. Following extensive discussion about the design and with the four design principles in mind, the following mock-up of the tank was produced:
\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{img/mock_design}
    \caption{Initial Mock-Up of the Autonomous Vehicle annotating each component}
\end{figure}

\subsection{Components Chosen}
\subsubsection{Arduino}
The Arduino is an open-source microcontroller which is easy to prototype with making it a flexible platform for both hardware and software projects.\\

The microcontroller on the board is programmed using the Arduino programming language, which is based on Wiring. 
The Arduino development environment is provided to allow interaction with the device, such as loading code. Arduino projects are able to communicate with software running on a computer or can simply run programs loaded onboard.\\

\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{img/arduino_uno}
    \caption{Arduino Uno board}
\end{figure}
Specifications (Uno)\footnote{http://arduino.cc/en/Main/ArduinoBoardUno}:\\
\begin{tabular}{ll}
    \textbf{Microcontroller} & ATmega328\\
    \textbf{Operating Voltage} & 5V\\
    \textbf{Input Voltage (Recommended)} & 7-12V\\
    \textbf{Input Voltage (Limits)} & 6-20V\\
    \textbf{Digital I/O} & 14 (6 provide PWM)\\
    \textbf{Analog Input Pins} & 6\\
    \textbf{DC Current per I/O Pin} & 40mA\\
    \textbf{DC Current for 3.3V Pin} & 50mA\\
    \textbf{Flash Memory} & 32KB / 0.5KB bootloader\\
    \textbf{SRAM} & 2KB\\
    \textbf{EEPROM} & 1KB\\
    \textbf{Clock Speed} & 16MHz \\
\end{tabular} \\

The Arduino was chosen for two primary reasons. First, the Arduino is a microcontroller and is designed specifically to interact with other electrical components in a usable manner. As there is a large number of electrical components utilised in the tank, this proved to be an indispensable feature during implementation. Secondly, the Arduino is an extremely popular microcontroller platform and there is a wealth of shield components, example code, and tutorials to learn with. This reason will not only boost productivity but also improve the quality of the final tank build.
\clearpage

\subsubsection{Raspberry Pi}
The Raspberry Pi is a small, single-board computer which is built on the ARM11 architecture and the ARMv6k 32-bit instruction set. The Raspberry Pi foundation provides users with Debian and Arch Linux ARM distributions for download in-order to use the device. It is a popular hobbyist platform and provides a large range of functionality for controlling components or carrying out specific software tasks. \\

\begin{figure}[H]
    \centering
    \includegraphics[width=0.35\textwidth]{img/rpi}
    \vspace{-10pt}
    \caption{Raspberry Pi Model B}
\end{figure}
Specifications (Model B)\footnote{http://downloads.element14.com/raspberryPi1.html}:\\
\begin{tabular}{ll}
    \textbf{Chip} & Broadcom BCM2835\\
    \textbf{CPU} & ARM1176JZ (700MHz)\\
    \textbf{GPU} & Dual Core VideoCore IV\\
    \textbf{Memory} & 512MB SDRAM\\
    \textbf{Ethernet} & Onboard 10/100 RJ45 Jack\\
    \textbf{USB 2.0} & Dual USB Connector\\
    \textbf{Video Output} & HDMI, Composite RCA\\
    \textbf{Audio Output} & 3.5mm jack, HDMI\\
    \textbf{Onboard Storage} & SD, MMc, SDIO card slot\\
    \textbf{Operating System} & Linux
\end{tabular} \\

Inclusion of the Raspberry Pi was required due to the need to intelligently and autonomously drive the Arduino via a Strategy so that SATan(k) can react appropriately to different environmental obstacles. In addition, the Raspberry Pi is computationally more able than the Arduino allowing the readings to be collated and transmitted, via WiFi, to a local area network machine without over-stressing the Arduino. While it was considered to remove the WiFi transmission entirely, it was ultimately decided that, with "Good Design Demands Good Compromise" in mind, it would be in the best interest of quality to include the transmission as rendering a map on a Raspberry Pi would be a task too computationally intensive for it to perform in addition to the tasks it is already performing. 

\subsubsection{HC-SR04 ultrasonic sensor}
The HC-SR04 ultrasonic sensor uses sonar, similar to bats or dolphins, to determine the distance from it to a particular object. It comes complete with a transmitter and receiver module in one removing the requirement to acquire additional hardware. The range of the sensor is 2cm to 400cm\footnote{Pg. 3 - HC-SR04 Users Manual, Cryton Technologies, May 2013.}, which fits our requirements to produce a map.\\

\begin{figure}[H]
    \centering
    \includegraphics[width=0.3\textwidth]{img/sonar_sensor}
    \caption{HC-SR04 ultrasonic sensor}
\end{figure}
\begin{tabular}{ll}
    \textbf{Power Supply} & +5V DC\\
    \textbf{Effectual Angle} & $<$15°\\
    \textbf{Resolution} & 0.3 cm\\
    \textbf{Measuring Angle} & 30 degree\\
    \textbf{Trigger Input Pulse width} & 10uS\\
    \textbf{Dimension} & 45mm x 20mm x 15mm\\
    \textbf{Operating Voltage} & 4.50-5.5V\\
    \textbf{Quiescent Current} & 1.5-2.5mA\\
    \textbf{Working Current} & 10-20mA\\
    \textbf{Ultrasonic Frequency} & 40kHz
\end{tabular}
\subsubsection{Other options considered}
\begin{enumerate}
    \item
        LIDAR(Laser radar) was an option considered following analysis of the
        ultrasonic prototype after concerns were raised over the accuracy of
        the readings. However, further reasearch showed that the
        availability of such a device is limited and its financial cost
        prohibited severely this option. If the project was to a commercial product, the
        initial investment in purchasing a LIDAR module would have been worth it.
    \item
        Physical actuators were considered for accurate range measuring.
        However, this was quickly dismissed as impractical as it would
        require additional apparatus being attached to the gadget and this
        could cause SATan(k) to knock an object over or affect its movement capabilities.
\end{enumerate}

\subsection{Initial Architecture and Software Approach}
\subsubsection{Initial Architecture}
Following the agreement to pursue an autonomous tank which maps its environment using sonar, work was done on producing a simple architecture of the system in terms of the components involved. As "Make the Common Case Fast" encourages making the primary functionality of a system as simple and fast as possible, the architecture produced focused entirely on the flow of the sonar readings: \\

\begin{figure}[H]
    \centering
    \includegraphics[width=1\textwidth]{img/architecture}
    \caption{Initial architecture demonstrating the interactions between each component.}
\end{figure}

As Figure 4 demonstrates, there are four main components involved in producing a map of the environment: the HC-SR04 sonar sensor to take the readings, the Arduino to collect and pass on these readings, a Raspberry Pi to command the Arduino and organise the readings, and a local area network computer to compute and render the readings into a map. In addition to these, the Arduino will drive two DC motors for movement and four servo motors to move the sensors to provide more accurate readings. This architecture would be the basis of all prototypes and the overall goal for the system, make the interactions between the four main components simple and ensure regular result production. \\

Other architectures considered consisted of removing the local area network machine entirely so that there was no transmission over a network. However, due to the computational intensity of rendering a graphical map, this was quickly disregarded as infeasible due to the limited capabilities of the Raspberry Pi. Another architecture considered was to use the ChipKit \footnote{ChipKit Pi - http://chipkit.net/wpcproduct/chipkit-pi/} shield with the Raspberry Pi to mimic the functionality of an Arduino in an attempt to remove the I/O bottleneck between them. However, upon investigation of this product, it was discovered that this particular shield limits the range of functionality available to a traditional Arduino board and it introduced a number of errors during simple prototypes. It was subsequently decided that this limitation would cause numerous issues for development in the future and use of the ChipKit Pi was not included in the final product.

\subsubsection{Initial Software Approach}
Initial work on the system's software began with an evaluation of programming languages to select the most advantageous for the project. The "slave" program running on the Arduino was, by the nature of the Arduino platform, limited to using the Arduino's own programming language, environment, and tools. For the "master" program, to be run on the Raspberry Pi, numerous options were considered. The primary candidates included C and C++, suggested primarily for performance concerns and the large amount of control of the computer these languages offer, Rust, for its safety guarantees while offering similar control to C, and Java, for familiarity and extensive library availability. Ultimately Java was selected due to its high-level nature and, most importantly, the group's familiarity with the language and associated tools and libraries.

For initial experiments and prototypes, Java software running on a PC sent commands to the Arduino via its USB serial interface  which the Arduino would then carry out and return result values. This style of system proved successful and it was decided to continue to use this style for later prototypes and eventually into creation of the final software build. \\

Alternative systems were considered led by software concerns, including amalgamating the Arduino and the Raspberry Pi into a single unit (Raspberry Pi only) to reduce serial communication overheads and avoid the somewhat limited Arduino software tools. However the feasibility of this system was impacted by the lack of voltage protection circuits on the Raspberry Pi, which are present on the Arduino, and the requirements for a system of this type to be multi-threaded which was perceived to be a potential issue on the Raspberry Pi's fairly weak, single-core CPU. \\

At this stage, the map rendering software was left mostly unconsidered, focussing primarily on ensuring the key sonar and movement code would be successful. However, for similar reasons as described above, it was agreed this software would be realised using Java. 

