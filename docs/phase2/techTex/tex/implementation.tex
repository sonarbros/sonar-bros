\section{Implementation}
\subsection{Hardware}
\subsubsection{Design/Changes}
The prototypes carried out enabled the group to discover the limitations of the components that were selected as well as methods to drive them. This prototyping provided a scope to work with on  where each component should be placed and how they will be used. The sensors were placed in the center of each side of the tank as this enabled reading from all angles for better accuracy. Each of the sensors were attached T-shaped harnesses which themselves were attached to servo motors. This enabled the sensors to move 180 degrees so that they could capture as much information about their environment as possible. \\

Once each component was evaluated, assembly of the tank began. The Tamiya tank kit was used for the chassis of the tank.\\

\begin{figure}[width=0.45\textwidth]
    \centering
    \includegraphics[width=0.45\textwidth]{img/tankkit}
    \caption{Tamiya tank kit.}
\end{figure}

This kit included a base plate on which the gear box, motors, and the tracks could be mounted. The construction of the tank consisted of following the kit instructions and, once complete, it was decided that testing the robustness of this chassis would be the best choice before continuing implementation. The tank performed several tests, mainly movement based, which would show any limitations with the tank. Soon after these tests, it was discovered that the tank slightly steered to the right.\\

The steering issue would mean inaccurate positioning which would result in the tank no idea of its position. After a group discussion, it was decided that a caster wheel trike model would be followed to replace the inaccurate tank track model. Initial testing of this model proved promising and resulted in more accurate manoeuvring. \\

\begin{figure}[width=0.45\textwidth]
    \centering
    \includegraphics[width=0.45\textwidth]{img/tankwheel}
    \caption{Image of the tricycle setup.}
\end{figure}

The next step was to attach a second level to the tank which would provide a platform for the other components. The initial design was to have a Raspberry Pi, Arduino, 4 servo motors and 4 sensors, but due to the change on manoeuvring another servo was added to the top level.\\

\begin{figure}
    \centering
    \begin{subfigure}{.5\textwidth}
        \includegraphics[width=.7\linewidth]{img/mock_design}
        \caption{Initial design of the tank}
        \label{fig:sub1}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \includegraphics[width=.7\linewidth]{img/posttank}
        \caption{New design of the tank}
        \label{fig:sub2}
    \end{subfigure}
    \caption{Initial and new designs of the tank}
    \label{fig:test}
\end{figure}

The image above displays both the initial design (left) and the final design (right) clearly showing all of the change made which were discussed above. As explained above, there were a number of changes from the original design and all of these changes had a positive impact on both the hardware and software aspects of the system. To make software implementation easier, final testing on the assembly of the tank was carried out and was therefore deemed suitable. \\

\begin{figure}[width=0.45\textwidth]
    \centering
    \includegraphics[width=0.7\textwidth]{img/circuitdiagram}
    \caption{Image of the circuit diagram.}
\end{figure}

Above is the final circuit diagram of the tank and for more information about each components, please see the components chosen section.\\

\pagebreak

\subsection{Software}
From the success of the software architecture used in the prototypical systems, it was decided that the same two level architecture would be utilised in the final build of the tank.

\subsubsection{Arduino}
The Arduino software, implemented using the Arduino's own programming language, is responsible for the control of the vehicle's motors, operating the sonar sensors, and returning averaged results over hundreds of sonar readings to the control program. It consists primarily of an infinite loop, which reads a command from the serial connection, parses the command and its arguments, then executes the requested operation and returns the result (if any).  This software took advantage of the Adafruit MotorShield library, which simplified the operation of the vehicle's motors and servos, which greatly reduced the complexity of the software and the time required to implement it. \\

There were a number of difficulties encountered in the implementation of this software component. The Arduino language's standard library is fairly sparse and does not contain many of the features implemented in higher-level languages. This particular issue was apparent in the implementation of the command-response system when String manipulation code was required and had to be implemented by hand (in the case of string splitting). Another difficulty was the lack of control over what was sent over the serial port. The Adafruit MotorShield library transmitted debug messages over the serial, which had to be worked around in the control protocol to ensure the control program only read relevant messages. To work around limitations of the vehicle's hardware - for example, the vehicle consistently driving to the left when told to drive in a straight line - it became apparent that many aspects of the hardware would have to be controlled independently (such as driving the left motor at a slightly higher speed than the right). The communications code was designed to allow the control of these factors at runtime, which added much of the complexity of the Arduino's softare. However, this system allowed the Arduino code to be mostly unchanged ie. avoided frequent recompilation and upload of the code to modify constant variables.

\subsubsection{Vehicle Control}
The Java control code that was to be run on the Raspberry Pi followed a similar design to the control code used in the prototypes. It made use of, as mentioned previously, the JSSC library for serial communications with the Arduino. Vehicle operations were abstracted into a class which issued commands to the Arduino over the JSSC serial socket. This high-level abstraction aided the implementation of the algorithms which control the tank. Many control algorithms were developed for testing purposes, however the algorithm used in the final program was the \textit{AutonomousMappingAlgorithm}. This algorithm performs basic obstacle avoidance and moves at fixed unit steps, taking sonar readings at each step. After each reading, the results were transformed into logical unit points and transmitted to the rendering program via a TCP socket. \\

The software was designed with software engineering best practises in mind; the major components are de-coupled so that the high-level algorithms be re-used with a very different vehicle with minimal changes. Correspondingly, this design made it very easy to substitute different algorithms and greatly increasing the speed different tests could be performed on the vehicle. The major difficulty faced in the implementation of this program was with serial communications. Due to the unique nature of the Arduino's bootloader, interacting with serial communications was difficult to guarantee a robust connection to the Arduino. This issue was worked around however by initiating a serial connection, then immediately closing it, effectively resetting the Arduino. Subsequently, a new connection is initiated, which is used as the communications channel for the rest of the program. Though the de-coupled algorithms system was designed to very easily allow swapping of vehicle behaviours, each time the behaviour was changed the program had to be re-compiled, packed into a new Jar file and transmitted to the Raspberry Pi on the vehicle. This could have been avoided using a scripting system or dynamic loading of algorithm classes, however the increased complexity in the software this would cause wasn't justified for the project.

\subsubsection{Map Rendering}
The rendering program, which receives the co-ordinates of objects sensed by the sonar and displays a graphical map corresponding to the environment the vehicle is running in was implemented in Java, made use of the Swing GUI/graphics library. It uses a \textit{ServerSocket} to listen for incoming communications from the Raspberry Pi, it then receives communications over the opened TCP socket and adds the point to a set held in memory. The graphical component of this code runs in a different thread which then draws the points in memory onto a Swing canvas every second. The threaded design ensures the map continuously updates and the vehicle isn't stopped when waiting to transmit points to be rendered while the graphics are re-drawn. It was essential for this program to be highly robust and was therefore implemented to gracefully handle clients closing connections unexpectedly. The robust nature of this program also made the transmission code in the Raspberry Pi's program much less complex.

\subsubsection{Calibration Tools}
Two tools were developed to aid in the calibration of the vehicle's many configurable factors. Initially, a graphical program was developed which allowed the user to input all of the motion/turning/speed factors and upload them to the vehicle (re-using the serial controlled tank code from the Raspberry Pi's control program). This program also performed rudimentary bounds checking to ensure the vehicle would not be damaged by dangerous inputs and avoided the chance of overflowing a variable, which had occurred several times earlier in testing providing unexpected and difficult to debug results. Later in the process another calibration tool was developed, this time using a text interface as the tests were now being run on the embedded hardware and no graphical environment was easily available - therefore, the only way to communicate with the Raspberry Pi at this point was over an SSH connection. This tool took the form of a "wizard" which guides the user through the process of calibrating the vehicle. At each stage the vehicle performed an action - for example, attempting to turn 90 degrees left - and the user is asked whether the action had been completed successfully - had turned too far, not far enough, had performed the action correctly, or a "not sure" option, the program acted on this input by multiplying the tested factor by $1.1$ or $0.9$ and the process would be continued until the user said the action had been performed correctly. In the case of a "not sure" response, the action would be repeated with the same factor. The wizard also provides a menu that allows the user to select which test to be run, enabling easy repeated testing of vehicle behaviours. These programs were both implemented in Java and greatly increased the speed with which the vehicle could be configured to work in new environments or react to hardware changes.

\subsubsection{Build System \& Tools}
With a software system of this size and complexity, it was decided to make use of a build system to automate the compilation of the project. The Ant build system technology was used to quickly compile and package the major Java software components. An extension to Ant, called Ivy, was used to allow easy downloads of the libraries used in the project which avoided checking in large library files to source control and avoided potential complications of different group members using different library versions etc. Git source control was used throughout the project, controlling both the source code and documentation of the project.

